import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.mortbay.log.Log;

public class KMeansMapper extends MapReduceBase implements
		Mapper<LongWritable, Text, IntWritable, Point4D> {

	private int dimension = 2, k;
	private Configuration fsconfig = new Configuration();
	private Point4D[] centroids;

	@Override
	public void configure(JobConf job) {
		// get the dimension
		dimension = Integer.parseInt(job.get("dimension"));

		// get the K
		k = Integer.parseInt(job.get("k"));

		centroids = new Point4D[k];

		// get the centroids
		String path = job.get("centroids");

		try {
			URI uri = new URI(job.get("uri"));
			FileSystem fileSystem = FileSystem.get(uri, fsconfig);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					fileSystem.open(new Path(path))));

			String line = "";
			for (int i = 0; i < k; i++) {
				// read line
				line = reader.readLine().split("\t")[1];

				Log.info("centroid line " + line);

				// parse the line
				String tokens[] = line.split(",");

				// read the point
				if (dimension == 1) {
					centroids[i] = new Point4D(Double.parseDouble(tokens[0]));
				} else if (dimension == 2) {
					centroids[i] = new Point4D(Double.parseDouble(tokens[0]),
							Double.parseDouble(tokens[1]));
				} else if (dimension == 3) {
					centroids[i] = new Point4D(Double.parseDouble(tokens[0]),
							Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]));
				} else {
					centroids[i] = new Point4D(Double.parseDouble(tokens[0]),
							Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]),
							Double.parseDouble(tokens[3]));
				}
			}
		} catch (Exception o) {
			System.out.println("Error " + o.getMessage());
		}
	}

	@Override
	public void map(LongWritable key, Text line,
			OutputCollector<IntWritable, Point4D> reducerOutput, Reporter reporter)
			throws IOException {

		// read line from file
		// Log.info("Line Read: " + line);

		if (line.toString().equals(""))
			return;

		// parse the line
		String tokens[] = line.toString().split(",");

		Point4D point = null;

		// read the point
		if (dimension == 1) {
			point = new Point4D(Double.parseDouble(tokens[0]));
		} else if (dimension == 2) {
			point = new Point4D(Double.parseDouble(tokens[0]), Double.parseDouble(tokens[1]));
		} else if (dimension == 3) {
			point = new Point4D(Double.parseDouble(tokens[0]), Double.parseDouble(tokens[1]),
					Double.parseDouble(tokens[2]));
		} else {
			point = new Point4D(Double.parseDouble(tokens[0]), Double.parseDouble(tokens[1]),
					Double.parseDouble(tokens[2]), Double.parseDouble(tokens[3]));
		}

		double d[] = new double[k];

		// now calculate distance vector
		for (int i = 0; i < k; i++) {
			d[i] = point.measure(centroids[i]);
		}

		int cluster = -1;
		double small = 100000;

		// see which cluster the point belong to, i.e 0 based
		for (int i = 0; i < d.length; i++) {
			if (d[i] < small) {
				small = d[i];
				cluster = i;
			}
		}

		// log
		// Log.info("This point " + point.toString() + " belongs to cluster " + cluster);

		// send output to collector
		reducerOutput.collect(new IntWritable(cluster), point);
	}

}
