import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

public class KMeansJob {

	public static int MAX = 99;

	public static enum Flag {
		CONVERGED
	}

	public static void main(String[] args) throws IllegalArgumentException, IOException,
			ClassNotFoundException, InterruptedException {

		// set the prefix
		String prefix = "hdfs://localhost:9090";

		RunningJob job;
		JobConf conf = new JobConf(KMeansJob.class);

		// set reduce output key and value
		conf.setOutputKeyClass(IntWritable.class);
		conf.setOutputValueClass(Point4D.class);

		// set the mapper and reducer
		conf.setMapperClass(KMeansMapper.class);
		conf.setReducerClass(KMeansReducer.class);

		// set input and output format
		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		// set the number of dimension
		conf.setInt("dimension", 4);
		// set K
		conf.setInt("k", 3);
		// set uri
		conf.set("uri", prefix + "/");

		int iteration = 0;

		// set the input file
		FileInputFormat.setInputPaths(conf, new Path(prefix + "/kmeans/iris.data"));

		// set centroid file path
		conf.set("centroids", prefix + "/koutput/centroids");
		FileOutputFormat.setOutputPath(conf,
				new Path(prefix + "/koutput/centroid" + String.format("%02d", iteration)));

		job = JobClient.runJob(conf);
		job.waitForCompletion();

		int converged = (int) job.getCounters().findCounter(Flag.CONVERGED).getValue();

		while (iteration < MAX && converged < 1) {
			// set centroid file path
			conf.set("centroids", prefix + "/koutput/centroid" + String.format("%02d", iteration)
					+ "/part-00000");

			// increment
			iteration++;

			FileOutputFormat.setOutputPath(conf,
					new Path(prefix + "/koutput/centroid" + String.format("%02d", iteration)));

			job = JobClient.runJob(conf);
			job.waitForCompletion();

			// get counter
			converged = (int) job.getCounters().findCounter(Flag.CONVERGED).getValue();
		}

		System.out.println("end");
	}
}
