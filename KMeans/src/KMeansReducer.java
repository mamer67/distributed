import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.mortbay.log.Log;

public class KMeansReducer extends MapReduceBase implements
		Reducer<IntWritable, Point4D, IntWritable, Point4D> {

	private int dimension = 2, k;
	private Configuration fsconfig = new Configuration();
	private Point4D[] centroids;

	private ArrayList<Point4D>[] points;
	private int done = 0;

	@SuppressWarnings("unchecked")
	@Override
	public void configure(JobConf job) {
		// get the dimension
		dimension = Integer.parseInt(job.get("dimension"));

		// get the K
		k = Integer.parseInt(job.get("k"));

		// set the array
		points = new ArrayList[3];

		for (int i = 0; i < points.length; i++) {
			points[i] = new ArrayList<Point4D>();
		}

		centroids = new Point4D[k];

		// get the centroids
		String path = job.get("centroids");

		try {
			URI uri = new URI(job.get("uri"));
			FileSystem fileSystem = FileSystem.get(uri, fsconfig);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					fileSystem.open(new Path(path))));

			String line = "";
			for (int i = 0; i < k; i++) {
				// read line
				line = reader.readLine().split("\t")[1];

				Log.info("centroid line " + line);

				// parse the line
				String tokens[] = line.split(",");

				// read the point
				if (dimension == 1) {
					centroids[i] = new Point4D(Double.parseDouble(tokens[0]));
				} else if (dimension == 2) {
					centroids[i] = new Point4D(Double.parseDouble(tokens[0]),
							Double.parseDouble(tokens[1]));
				} else if (dimension == 3) {
					centroids[i] = new Point4D(Double.parseDouble(tokens[0]),
							Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]));
				} else {
					centroids[i] = new Point4D(Double.parseDouble(tokens[0]),
							Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]),
							Double.parseDouble(tokens[3]));
				}
			}
		} catch (Exception o) {
			System.out.println("Error " + o.getMessage());
		}
	}

	@Override
	public void reduce(IntWritable key, Iterator<Point4D> values,
			OutputCollector<IntWritable, Point4D> fileOutput, Reporter reporter) throws IOException {

		Point4D newCentroid = new Point4D(0, 0, 0, 0);
		ArrayList<Point4D> buffer = new ArrayList<Point4D>(100);

		// loop on the values and calculate new centroid for each cluster
		while (values.hasNext()) {
			Point4D p = new Point4D(values.next());
			newCentroid.add(p);
			buffer.add(p);
		}

		// get the average
		newCentroid.average();

		// decide either to stop or continue
		if (newCentroid.equal(centroids[key.get()])) {
			Log.info(String.format("new C: %s and old C: %s", newCentroid, centroids[key.get()]));

			// set the points
			points[key.get()].clear();
			for (int i = 0; i < buffer.size(); i++) {
				points[key.get()].add(buffer.get(i));
			}

			//increment
			done += 1;

			Log.info(key.get() + "-" + done);
		}

		Log.info(key.get() + "/" + done);

		// log
		Log.info("centroid is " + newCentroid.toString());

		// write centroid to output
		fileOutput.collect(key, newCentroid);

		if (done >= k) {
			// write points
			for (int i = 0; i < points.length; i++) {
				for (int j = 0; j < points[i].size(); j++) {
					fileOutput.collect(new IntWritable(i), points[i].get(j));
				}
			}

			// increment reporter
			reporter.incrCounter(KMeansJob.Flag.CONVERGED, 1);
		}
	}
}
