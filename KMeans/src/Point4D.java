import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class Point4D implements Writable {

	public static double epis = 10e-5;

	private double n1, n2, n3, n4;
	private double s1, s2, s3, s4;
	private int num = 1;

	public Point4D() {

	}

	public Point4D(Point4D p) {
		this.n1 = p.n1;
		this.n2 = p.n2;
		this.n3 = p.n3;
		this.n4 = p.n4;

		this.s1 = p.n1;
		this.s2 = p.n2;
		this.s3 = p.n3;
		this.s4 = p.n4;
	}

	public Point4D(double n1, double n2, double n3, double n4) {
		this.n1 = n1;
		this.n2 = n2;
		this.n3 = n3;
		this.n4 = n4;

		this.s1 = n1;
		this.s2 = n2;
		this.s3 = n3;
		this.s4 = n4;
	}

	public Point4D(double n1, double n2, double n3) {
		this.n1 = n1;
		this.n2 = n2;
		this.n3 = n3;

		this.s1 = n1;
		this.s2 = n2;
		this.s3 = n3;
	}

	public Point4D(double n1, double n2) {
		this.n1 = n1;
		this.n2 = n2;

		this.s1 = n1;
		this.s2 = n2;
	}

	public Point4D(double n1) {
		this.n1 = n1;

		this.s1 = n1;
	}

	/**
	 * Measure distance between two points
	 * @param c
	 * @return measure
	 */
	public double measure(Point4D c) {
		return Math.sqrt(Math.pow(n1 - c.n1, 2) + Math.pow(n2 - c.n2, 2) + Math.pow(n3 - c.n3, 2)
				+ Math.pow(n4 - c.n4, 2));
	}

	/**
	 * Sum points
	 * @param p
	 */
	public void add(Point4D p) {
		s1 += p.n1;
		s2 += p.n2;
		s3 += p.n3;
		s4 += p.n4;

		// increment number
		num++;
	}

	/**
	 * Get the average
	 * @return
	 */
	public void average() {
		n1 = s1 / num;
		n2 = s2 / num;
		n3 = s3 / num;
		n4 = s4 / num;
	}

	public boolean equal(Point4D p) {
		if (Math.abs(n1 - p.n1) > epis)
			return false;
		if (Math.abs(n2 - p.n2) > epis)
			return false;
		if (Math.abs(n3 - p.n3) > epis)
			return false;
		if (Math.abs(n4 - p.n4) > epis)
			return false;

		return true;
	}

	public String toString() {
		return String.format("%f,%f,%f,%f", n1, n2, n3, n4);
	}

	public double getX1() {
		return n1;
	}

	public void setX1(double x1) {
		this.n1 = x1;
	}

	public double getX2() {
		return n2;
	}

	public void setX2(double x2) {
		this.n2 = x2;
	}

	public double getY1() {
		return n3;
	}

	public void setY1(double y1) {
		this.n3 = y1;
	}

	public double getY2() {
		return n4;
	}

	public void setY2(double y2) {
		this.n4 = y2;
	}

	@Override
	public void readFields(DataInput input) throws IOException {
		String line = input.readLine();
		// parse the line
		String tokens[] = line.split(",");
		int dimension = tokens.length;

		// read the point
		if (dimension == 1) {
			n1 = Double.parseDouble(tokens[0]);
		} else if (dimension == 2) {
			n1 = Double.parseDouble(tokens[0]);
			n2 = Double.parseDouble(tokens[1]);
		} else if (dimension == 3) {
			n1 = Double.parseDouble(tokens[0]);
			n2 = Double.parseDouble(tokens[1]);
			n3 = Double.parseDouble(tokens[2]);
		} else {
			n1 = Double.parseDouble(tokens[0]);
			n2 = Double.parseDouble(tokens[1]);
			n3 = Double.parseDouble(tokens[2]);
			n4 = Double.parseDouble(tokens[3]);
		}
	}

	@Override
	public void write(DataOutput output) throws IOException {
		output.write(String.format("%f,%f,%f,%f\n", n1, n2, n3, n4).getBytes());
	}

}
