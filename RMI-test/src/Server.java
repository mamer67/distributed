import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server{
	public static void main(String[] args) throws IOException,
			AlreadyBoundException {
		System.setProperty("java.rmi.server.hostname", "whitechimera.no-ip.biz");
		Item item = new myItem("s");
//		Item2 item2 = new myItem2();
		Registry reg = LocateRegistry.createRegistry(9090);
		reg.rebind("Item", item);
//		reg.rebind("Item2", item2);
	}
}
