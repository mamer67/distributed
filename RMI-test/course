BABYL OPTIONS:
Version: 5
Labels:
Note:   This is the header of an rmail file.
Note:   If you are seeing it in rmail,
Note:    it means the file has no messages in it.

0, unseen,,
*** EOOH ***
Newsgroups: comp.os.mach
From: eric@cs.sfu.ca (Eric Kolotyluk)
Subject: An Advanced Course on Distributed Systems, Reminder, Long
Organization: Simon Fraser University, Burnaby, B.C., Canada
Date: Wed, 17 Mar 1993 02:18:24 GMT

repy to: van93@cs.sfu.ca


                  	REMINDER -- ANNOUNCEMENT 
                 	------------------------

           	AN ADVANCED COURSE ON DISTRIBUTED SYSTEMS

	           VANCOUVER, BRITISH COLUMBIA, CANADA          

	               JUNE 23RD - JULY 2ND, 1993

(Update: Presidents Bill Clinton and Boris Yeltsin have chosen Vancouver for 
their World Summit on April 3-5 1993. Their meeting will be held one block
from our Course Location).

Vancouver '93 is the sixth offering of the Advanced Course on 
Distributed Systems.  The five previous offerings were held in Estoril, 
Portugal ('92), Kariuzawa, Japan ('91), Bologna, Italy ('90), Ithaca, 
New York ('89), and Troms , Norway ('88).  The objective of the 
course is to familiarize practitioners and researchers with key issues 
in distributed systems.  The course will also be relevant to Engineers, 
Scientists and Educators who wish to update their knowledge of 
Distributed Systems.  Attendees are assumed to have familiarity with 
basic operating systems concepts.  The lectures will discuss the 
fundamental problems of the area, review known solutions and 
paradigms, and show how to apply known theoretical results to the 
design of practical systems.  Vancouver '93 lecturers are 
internationally-known researchers whose interests and experiences 
scan the full range of distributed computing.

SPONSORSHIP:  
The course is sponsored by Simon Fraser University's Centre for 
Systems Science, the Faculty of Applied Science, the School of 
Computing Science, and British Columbia's Advanced Systems Institute.

FORMAT:
This course will be organized as a series of daily lectures with several 
discussion sections.  The discussions will permit small groups of 
attendees to interact directly with lecturers, either to focus on issues 
that arise during the lectures or to pursue other topics of interest to the
group.  

			--- THE LECTURERS ---

M. Stella Atkins is an Associate Professor of Computing Science at 
Simon Fraser University, Vancouver, Canada.  She is interested in 
integrating language and systems design for distributed applications 
such as medical image reconstruction and distributed sorting.  She has 
been involved with the development and performance of the high level 
distributed programming language SR (Synchronising Resources), and with
the development and use of object-oriented dataflow techniques for 
image analysis.
                        
Ozalp Babaoglu is a Professor of Computer Science at the University 
of Bologna, Italy where he leads the Paralex project whose goal is to 
design and implement an integrated environment for programming, 
debugging, monitoring, and controlling reliable distributed 
applications.  Babaoglu was a principal designer and implementor of 
Berkeley UNIX.  He is an editor for the Springer-Verlag journal 
"Distributed Computing".

Hermann Kopetz is a Professor of computer science at the Technical 
University of Vienna, Austria.  After spending eight years in industry 
in the field of computer process control he has been involved in 
research on real time and fault tolerance in distributed computing 
systems for the past 10 years.  Dr. Kopetz is the principal investigator 
of the MARS distributed fault-tolerant real-time system.  He has 
published many papers on real-time systems, clock synchronization, 
fault-tolerance and software engineering.  He has been the chairman 
of the IEEE Technical Committee on Fault Tolerant Computing for 
the past two years.

Butler W. Lampson is a member of the research staff at Digital's 
Systems Research Center in Palo Alto, California and Cambridge 
Research Laboratory in Cambridge, Massachusetts.  He has worked 
on computer architecture, high speed networks, raster printers, page 
description languages and their semantics, programming in the large, 
fault-tolerant computing, computer security, and WYSIWYG editors.

Sape J. Mullender is Professor of systems programming and 
architecture in the Faculty of Computer Science of the University of 
Twente in the Netherlands, where he leads the Huygens research 
project on operating system support for distributed multimedia 
systems.  He is a principal designer of the Amoeba distributed 
operating system and he has published papers on file systems, high-
performance RPC protocols, locating migratable objects in computer 
networks, and protection mechanisms.

Roger M. Needham is a Professor of Computer Systems and head of 
the Computer Laboratory at the University of Cambridge, England 
and a Fellow of the Royal Society.  He has contributed extensively to 
many aspects of distributed computing and has most recently been 
interested in computer protection and security.

Mahadev Satyanarayanan is an Associate Professor of Computer 
Science at Carnegie Mellon University.  His research addresses the 
problem of data access in large-scale distributed systems.  He 
currently leads the Coda project, whose goal is to provide highly-
available distributed file access.  Earlier, he was a principal architect 
and implementor of the Andrew file system.

Fred B. Schneider is an Associate Professor of Computer Science at 
Cornell University.  His research is primarily concerned with 
methodology for designing and reasoning about concurrent 
programs, particularly fault-tolerant and distributed ones.  He is the 
managing editor of "Distributed Computing" and an editor for 
"Information Processing Letters", Springer-Verlag "Texts and 
Monographs in Computer Science", and IEEE "Transactions on Software
Engineering".
 
Michael D. Schroeder is a member of the research staff at Digital's 
Systems Research Center in Palo Alto, California.  His particular 
interest is discovering practical structures for distributed systems.
He has worked on computer protection and security, encryption-based 
authentication protocols, computer message systems, naming in large 
networks, remote procedure call performance, distributed file 
systems, and high-speed local area networks.

Sam Toueg is an Associate Professor Computer Science at Cornell 
University.  His research interests include distributed systems, fault-
tolerance, real-time systems, and distributed databases.

William E. Weihl is an Associate Professor of Computer Science of 
the Massachusetts Institute of Technology.  His research interests 
focus on parallel and distributed computing, particularly in the areas 
of programming methodology, programming languages, specification 
techniques, synchronization, and fault-tolerance.  He is one of the 
principal designers of the Argus and Mercury systems developed at 
MIT.

			--- COURSE OUTLINE ---

INTRODUCTION

Introduction to distributed systems (Schroeder)
Theory for practitioners (Schneider)

FUNDAMENTAL CONCEPTS

Fundamental concepts of distributed computing (Babaoglu)
Specifications of concurrent and distributed systems (Weihl)
Understanding non-blocking atomic commitment (Babaoglu & 
Toueg)
Replication management using the state-machine approach  
(Schneider)

COMMUNICATIONS

Interprocess communication  (Mullender)
Reliable messages and connection management (Lampson)
Design of switch-based LANs  (Schroeder)

SERVICES

Naming in distributed systems (Needham)
Transaction-processing techniques (Weihl)
Distributed file systems (Satyanarayanan)
Multimedia (Needham)

DISTRIBUTED SYSTEMS ARCHITECTURE

Language support for distributed systems (Atkins)
Kernel support for distributed systems (Mullender)
Operating-system support for distributed applications  (Atkins)
Support for parallel programs (Babaoglu)

SECURITY

Cryptography and secure channels (Needham)
Security in distributed systems (Lampson)

REAL TIME AND DEPENDABILITY

Real time and real-time systems (Kopetz)
Dependability and distributed fault tolerance (Kopetz)

			--- LOCATION AND CLIMATE ---

Vancouver is a scenic cosmopolitan centre bordered on one side by 
mountain wilderness and on another by ocean beaches, where you can 
ski alpine slopes in the morning and sail in the afternoon.  The course 
will be held at Simon Fraser University (SFU) Harbour Centre 
centrally located in downtown Vancouver within walking distance of 
many of the city's attractions such as Chinatown, historic Gastown, 
Stanley Park with its world-famous aquarium, and Granville Island 
Market offering farm-fresh produce, fresh seafood, and fine 
restaurants.  The Harbour Centre is also at the hub of Vancouver's 
excellent public transit system including the SeaBus ferry to the 
North Shore and the SkyTrain monorail across the city to the Fraser 
River.  Vancouver's temperature averages in the low twenties Celsius 
in June/July with nightly temperatures around twelve or thirteen 
Celsius.  Bring a raincoat or windbreaker as well as a sweater for cool 
evenings. 

			--- ACCOMMODATION ---

Upon completion of the enclosed form and payment of a room 
deposit, we will make reservations for the desired type of 
accommodation.  Reservations will be handled on a first-come-first-
served basis.  Hotel reservations and course registration deadline is 
April 15th, 1993.

Please note that all reservations require a deposit to cover the first 
night or credit card guarantee to hold room beyond 18:00 hours on 
your arrival date.  All hotel/hostel rates are quoted in Canadian funds.

Reservations made after the April 15th '93 deadline will be on a 
space-available basis.  Non-Canadian residents can claim a refund of 
the Goods and Services Tax (7%) on hotel accommodation at the 
Duty Free Stores at points of departure (both at the airport and at 
border crossings).

WATERFRONT CENTRE HOTEL
900 Canada Place Way, Vancouver, BC  Canada  V6C 3L5
Dial Toll Free Canada:  1-800-268-9411/USA:  1-800-828-7447
Reservations telephone:  (604)  691-1820;  FAX:  (604)  691-1828

Rates:  Cdn. $130 (single/double/twin) plus 10% provincial hotel tax 
and 7% GST.  Add $20 for each additional adult. 

The Waterfront Centre Hotel is the gateway to downtown 
Vancouver's waterfront, a five minute walk to SFU Harbour Centre.  
Ideally situated in the downtown core, on the shores of Burrard Inlet, 
within walking distance to the most historic part of the city, Gastown.  
Stanley Park and the Vancouver Aquarium is just a 15 minute walk 
away.  There is a heated outdoor pool and a fitness room.

BARCLAY HOTEL
1348 Robson Street, Vancouver, BC  Canada  V6E 1C5
Hotel Telephone:  (604) 688-8850;  Hotel FAX:  (604) 688-2534

Rates:  Cdn. $60 (single); $70 (twin) plus 10% provincial hotel tax 
and 7% GST.  Add  $10 for each additional adult.

A heritage hotel near downtown Vancouver, it is located on the city's 
popular and famous Robson Street, surrounded by charming 
boutiques, shops, bistros, restaurants and art galleries.  Within 
walking distance are Vancouver attractions - Stanley Park, English 
Bay, the financial district and major shopping malls...right in the 
center of activities.  It is an approximately 20 minute walk to SFU 
Harbour Centre.

HOTEL  AT THE Y
580  Burrard Street, Vancouver, BC  Canada  V6C 2K9
Dial Toll Free in British Columbia & Alberta only:  1-800-663-1424
Telephone:  (604) 662-8188;  FAX:  (604) 681-2550

Rates:  Cdn. $41 single (1 single bed with hall bathroom); Cdn. $46 
single (1 single bed with shared bath); Cdn. $58 double/twin (2 single 
beds with shared bath) plus 10% provincial hotel tax.  Other room 
configurations available.  

An economical alternative for families or students.  Restaurant, 
kitchen facilities (bring your own utensils), weight room, fitness 
classes, indoor pool (women only).  Approximately a 10 minute walk 
from SFU Harbour Centre.  Reservation can be made directly with the 
hotel by phone or FAX by April 15th, 1993.  Please mention that you 
are with the "Advanced Course on Distributed Systems".  


VANCOUVER INTERNATIONAL YOUTH HOSTEL
1515 Discovery Street, Vancouver, BC  Canada  V6R 4K5
Telephone:  (604) 224-3208; FAX:  (604) 224-4852

Rates:  Cdn. $12.50 (bunk bed style in dormitories)  No taxes 
applicable.  Guests either supply their own sleeping bag/sheets or 
linen are available for $1.50 per person.  Guests supply their own 
towels or may be rented for $ .50.

It is located at Jericho Beach and is a 30 minutes bus ride from 
downtown and SFU Harbour Centre.

			-- AIRPORT TRANSPORTATION ---

An Airport Express bus service runs from Vancouver International 
Airport to most of the downtown Vancouver hotels.  Buses depart 
from the pick-up area outside level 2 at Vancouver International 
Airport every 15 minutes.  Tickets can be purchased from the driver.  
These buses follow two routes.  Check with the customer sales 
representative before you board the bus to ensure that you are on the 
correct bus for your hotel.  Cost one way into Vancouver is Cdn. 
$8.25 or Cdn. $14 return (good any time).  This service operates 
between 6:15 and 24:15.  Taxis are also available; current cost is 
approximately Cdn. $25.00.  Travel time from the airport to 
downtown Vancouver is 35-45 minutes.  All major car rental 
companies are located at the airport.

		-- REGISTRATION  INFORMATION AND DEADLINES ---

Complete and mail your Course Tuition/Hotel Reservation form with 
tuition fee  and room deposit by April 15th, 1993. Please remember 
that US and international mail can take 14 - 21 days or longer.   You 
will be sent a receipt as confirmation of your registration.  No refunds 
will be issued after May 30th.  In case of low participation levels or 
serious difficulty in holding the course, the organizers reserve the 
right to cancel the course at full reimbursement of paid course fees.

TUITION FEES:
The tuition for the 10-day course is $1,800 Canadian or $1,300 
Canadian for full-time registered students and ASI members.  Student 
delegates should attach a letter certifying their status from the head of 
their department.  The fee includes all lectures and discussion 
sections, all course material including a copy of the textbook 
"Distributed Systems" edited by Sape J. Mullender, a welcome 
reception, all lunches and coffee breaks, a banquet, and a tour on the 
weekend.

METHOD OF PAYMENT:
Payment can be made by cheque or bank draft in Canadian dollars 
drawn on a Canadian bank or credit card (MasterCard and VISA 
only).  Cheques/bank drafts should be made payable to "Simon Fraser 
University - Van '93" and should accompany the Course 
Tuition/Hotel Reservation form.  Registrations will not be processed 
without fee payment.

An Advanced Course on Distributed Systems - Van93

Course Tuition /Hotel Registration Form

Please type or print:
Name:_____________________________________________________

Affiliation__________________________________________________

Address____________________________________________________

City_________________________Province or State________________

 Country___________________________Postal or Zip Code__________

Telephone_______________________FAX_______________________

E-Mail Address______________________________________________
    I am a full-time student; attached is a letter from my department 
chair.

Hotel Reservation:
    Waterfront Centre Hotel :     Single      Double      Twin
         (Deposit:  Cdn. $130 for single/double/twin)
            Smoking      Non-smoking
    Barclay Hotel:    Single       Twin
          (Deposit:  Cdn. $60 for single; $70  for twin)
    Hotel at the Y.  Please make reservation directly  by phone or FAX.    
    
           Please mention "Advanced Course on Distributed Systems".
    Cdn. Youth Hostel.  Bunk bed style.  (Deposit:  Cdn. $12.50)
With whom will you be sharing a room?__________________________
 Arrival Date:_________________Departure Date:__________________

Course Tuition Fee:
General Tuition                                        $1,800 Canadian 
Full-time students and ASI members       $1,300 Canadian
Payment is made by: 
     cheque (payable to Simon Fraser University-Van'93)
     Visa or    Mastercard
Credit Card Number____________________Expiry Date____________

Name of Cardholder__________________________________________


Signature___________________________________________________

Tuition:__________ Room Deposit:__________Total Amount:________

Return this form by April 15th, 1993 to:
                
                Advanced Course on Distributed Systems-Van'93  
                c/o  Vera Robinson                                              
     
                School of Computing Science                                  
                Burnaby, British Columbia  Canada
                V5A 1S6

                Phone:  (604) 941-9229
                FAX:    (604) 944-4205
                E-Mail:  van93@cs.sfu.ca

