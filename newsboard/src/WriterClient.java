import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class WriterClient {
	public static void main(String argv[]) {
		// String host = "localhost";// argv[0];
		// String port = "9090";// argv[1];
		// String id = "2";// argv[2];
		// String naccess = "3";// argv[3];

		String host = argv[0];
		String port = argv[1];
		String id = argv[2];
		String naccess = argv[3];

		System.out.println("rSeq sSeq");
		PrintWriter log = null;

		try {
			log = new PrintWriter(new FileWriter("log-client-" + id), true);
		} catch (IOException e1) {
			System.out.println("Error opening log file: " + e1.getMessage());
		}

		log.println("Client type: Writer");
		log.println("Client name: " + id);
		log.println("rSeq\t sSeq");

		int numberAccess = Integer.parseInt(naccess);

		// start clientSocket
		Socket clientSocket = null;
		String line = "";
		int requestNum = -1;
		int serviceNum = -1;

		for (int i = 0; i < numberAccess; i++) {
			try {
				clientSocket = new Socket(host, Integer.parseInt(port));
			} catch (Exception e) {
				System.out.println("Error in connecting to server: " + e.getMessage());
			}

			BufferedReader readMessage = null;
			DataOutputStream writeMessage = null;

			try {
				readMessage = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			} catch (IOException e) {
				System.out.println("Error in opening input stream: " + e.getMessage());
			}

			try {
				writeMessage = new DataOutputStream(clientSocket.getOutputStream());
			} catch (IOException e) {
				System.out.println("Error in opening output stream: " + e.getMessage());
			}

			// format "request=read&id=num"
			try {
				writeMessage.writeBytes("request=write&id=" + id + "\n");
			} catch (IOException e) {
				System.out.println("Error in writing Message: " + e.getMessage());
			}

			// format "rSeq sSeq"
			try {
				line = readMessage.readLine();
			} catch (IOException e) {
				System.out.println("Error in reading Message: " + e.getMessage());
			}
			String tokens[] = line.split(" ");

			requestNum = Integer.parseInt(tokens[0]);
			serviceNum = Integer.parseInt(tokens[1]);

			// write to log
			System.out.println(requestNum + " " + serviceNum);
			log.println(requestNum + "\t\t " + serviceNum);

			try {
				clientSocket.close();
			} catch (IOException e) {
				System.out.println("Error in closing connection: " + e.getMessage());
			}
		}
		
		log.close();
	}
}