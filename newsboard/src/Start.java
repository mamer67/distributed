import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;

public class Start {

	private static String host;
	private static int port;

	private static PrintWriter logServer, logReader, logWriter;

	public static void main(String[] args) {
		new Start().run();
	}

	public void run() {
		Parser parser = new Parser();

		// read server attributes
		host = parser.getServerName();
		port = parser.getPortNumber();

		try {
			logServer = new PrintWriter(new FileWriter("start-server"), true);
			logWriter = new PrintWriter(new FileWriter("start-writer"), true);
			logReader = new PrintWriter(new FileWriter("start-reader"), true);
		} catch (IOException e1) {
			System.out.println("Error opening log file: " + e1.getMessage());
		}

		Thread t = new Thread(new Upload(host, port, true, true, null, -1, -1, -1));
		t.start();

		int id = 1;
		int nA = parser.getNumberOfAccess();

		String[] readers = parser.getReaders();
		for (int i = 0; i < readers.length; i++) {
			t = new Thread(new Upload(readers[i], -1, false, true, host, port, id, nA));
			id++;
			t.start();
		}

		String[] writers = parser.getWriters();
		for (int i = 0; i < writers.length; i++) {
			t = new Thread(new Upload(writers[i], -1, false, false, host, port, id, nA));
			id++;
			t.start();
		}
	}

	class Upload implements Runnable {
		private String h;
		private int prt;

		private String remoteHost;
		private int remotePort;
		private int id;
		private int numberAccess;

		private String fileName = "";
		private boolean server, reader;

		public Upload(String host, int port, boolean server, boolean reader, String remoteHost, int remotePort, int id, int numberAccess) {
			h = new String(host);
			prt = port;
			fileName = "Server";
			this.server = server;
			this.reader = reader;

			Random r = new Random();
			if (!server) {
				try {
					Thread.sleep(r.nextInt(10) * 1000 + 20000);
				} catch (InterruptedException e) {
					System.out.println("Error in sleeping: " + e.getMessage());
				}

				this.remoteHost = remoteHost.split("/")[0].split("@")[1];
				this.remotePort = remotePort;
				this.id = id;
				this.numberAccess = numberAccess;

				if (reader) {
					fileName = "ReaderClient";
				} else {
					fileName = "WriterClient";
				}
			}
		}

		@Override
		public void run() {

			String[] tokens = h.split("/");
			String[] tokenPermission = tokens[1].split(";");

			Runtime r = Runtime.getRuntime();

			if (tokenPermission[0].equals("p")) {
				Process p = null;
				String line = "";
				try {
					line = "sshpass -p " + tokenPermission[1] + " scp src/" + fileName + ".java " + tokens[0] + ":~";
					p = r.exec(line);
					p.waitFor();
					// System.out.println(line);

					line = "sshpass -p " + tokenPermission[1] + " ssh -p 22 " + tokens[0] + " javac " + fileName + ".java";
					p = r.exec(line);
					p.waitFor();
					// System.out.println(line);

					if (server) {
						line = "sshpass -p " + tokenPermission[1] + " ssh -p 22 " + tokens[0] + " java " + fileName + " " + prt;
						p = r.exec(line);
						// System.out.println(line);
					} else {
						line = "sshpass -p " + tokenPermission[1] + " ssh -p 22 " + tokens[0] + " java " + fileName + " " + remoteHost + " " + remotePort + " " + id + " " + numberAccess;
						p = r.exec(line);
						// System.out.println(line);
					}

					System.out.println("connected to: " + tokens[0]);

					BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
					while (true) {
						if (br.ready()) {
							String ll = br.readLine();
							System.out.println(tokens[0] + ": " + ll);
							if (server) {
								logServer.println(ll);
							} else {
								if (reader) {
									logReader.println(ll);
								} else {
									logWriter.println(ll);
								}
							}
						}
					}
				} catch (Exception e) {
					System.out.println("Error executing cmd: " + e.getMessage());
				}
			} else if (tokenPermission[0].equals("i")) {
				String line = "";
				Process p = null;
				try {
					line = "scp -i " + tokenPermission[1] + " src/" + fileName + ".java " + tokens[0] + ":~";
					p = r.exec(line);
					p.waitFor();
					// System.out.println(line);

					line = "ssh -i " + tokenPermission[1] + " -p 22 " + tokens[0] + " javac " + fileName + ".java";
					p = r.exec(line);
					p.waitFor();
					// System.out.println(line);

					if (server) {
						line = "ssh -i " + tokenPermission[1] + " -p 22 " + tokens[0] + " java " + fileName + " " + prt;
						p = r.exec(line);
						// System.out.println(line);
					} else {
						line = "ssh -i " + tokenPermission[1] + " -p 22 " + tokens[0] + " java " + fileName + " " + remoteHost + " " + remotePort + " " + id + " " + numberAccess;
						p = r.exec(line);
						// System.out.println(line);
					}

					System.out.println("connected to: " + tokens[0]);

					BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
					while (true) {
						if (br.ready()) {
							String ll = br.readLine();
							System.out.println(tokens[0] + ": " + ll);
							if (server) {
								logServer.println(ll);
							} else {
								if (reader) {
									logReader.println(ll);
								} else {
									logWriter.println(ll);
								}
							}
						}
					}
				} catch (Exception e) {
					System.out.println("Error executing cmd: " + e.getMessage());
				}
			} else {
				// unvalid case
			}

		}
	}
}
