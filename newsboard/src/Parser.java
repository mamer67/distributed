import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Parser {
	private String serverName = "";
	private int port = 0;
	private int numberofReaders = 0, numberOfWriters = 0;
	private String[] readers = null, writers = null;
	private int lineNumber = 0;
	private int numberOfAccess = 0;

	public static void main(String[] args) throws FileNotFoundException {
		Parser p = new Parser();
		System.out.println("Readers " + p.getNumberOfReaders());
		for (int i = 0; i < p.getNumberOfReaders(); i++) {
			System.out.println(p.getReaders()[i]);
		}
		System.out.println("Writers");
		for (int i = 0; i < p.getNumberOfWriters(); i++) {
			System.out.println(p.getWriters()[i]);
		}
		System.out.println("POrt " + p.getPortNumber());
		System.out.println("Number of Access " + p.getNumberOfAccess());
		System.out.println("Server name " + p.getServerName());
	}

	public Parser() {
		Scanner scan = null;
		try {
			scan = new Scanner(new File("system.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		while (scan.hasNext()) {
			String line = scan.nextLine();
			if (line.split("=")[0].equals("RW.numberOfAccesses")) {
				numberOfAccess = Integer.parseInt(line.split("=")[1]);
				break;
			}
			if (lineNumber == 0) {
				String[] array = line.split("=");
				lineNumber++;
				serverName = array[1];
			} else if (lineNumber == 1) {
				port = Integer.parseInt(line.split("=")[1]);
				lineNumber++;
			} else if (lineNumber == 2) {
				numberofReaders = Integer.parseInt(line.split("=")[1]);
				readers = new String[numberofReaders];
				for (int i = 0; i < readers.length; i++) {
					line = scan.nextLine();
					readers[i] = line.split("=")[1];
				}
				lineNumber++;
			} else {
				numberOfWriters = Integer.parseInt(line.split("=")[1]);
				writers = new String[numberOfWriters];
				for (int i = 0; i < writers.length; i++) {
					line = scan.nextLine();
					writers[i] = line.split("=")[1];
				}
			}
		}
	}

	public int getPortNumber() {
		return port;
	}

	public int getNumberOfWriters() {
		return numberOfWriters;
	}

	public int getNumberOfReaders() {
		return numberofReaders;
	}

	public String[] getWriters() {
		return writers;
	}

	public String[] getReaders() {
		return readers;
	}

	public int getNumberOfAccess() {
		return numberOfAccess;
	}

	public String getServerName() {
		return serverName;
	}

}