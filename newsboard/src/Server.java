import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.Semaphore;

/*
 * Server class assumes no channel loss and reliable data streams
 * as its build on TCP which handles all this
 * */
class Server {
	private static ServerSocket welcomeSocket;
	private static PrintWriter logWriter;
	private static PrintWriter logReader;
	private static PrintWriter log;

	private static Integer newsBoard = -1;

	private static Integer requestSequence = 1;
	private static Integer serviceSequence = 1;

	// keep a list of all writers ids
	private static Random random = new Random();

	private Socket incomingConnection;

	private Semaphore x = new Semaphore(1);
	private Semaphore y = new Semaphore(1);
	private Semaphore z = new Semaphore(1);
	private Semaphore wsem = new Semaphore(1);
	private Semaphore rsem = new Semaphore(1);

	private int writeCount = 0, readCount = 0;

	// start_server in main
	public static void main(String argv[]) {
		new Server().start(argv);
	}

	public void start(String argv[]) {
		// read port number form argv[0]
		// String port = "9090";

		String port = argv[0];

		// open a log file
		try {
			logWriter = new PrintWriter(new FileWriter("server-log-writer"), true);
			logReader = new PrintWriter(new FileWriter("server-log-reader"), true);
			log = new PrintWriter(new FileWriter("server-log"), true);
		} catch (IOException e1) {
			System.out.println("Error opening log file: " + e1.getMessage());
		}

		System.out.println("sSeq oVal ID rNum");
		logWriter.println("Writers:");
		logWriter.println("sSeq\t oVal\t ID\t rNum");

		logReader.println("Readers:");
		logReader.println("sSeq\t oVal\t ID\t rNum");

		log.println("sSeq\t oVal\t ID\t rNum");

		// add ctrl-c catcher
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				logWriter.close();
				logReader.close();
				log.close();
			}
		}));

		// open server connection
		try {
			welcomeSocket = new ServerSocket(Integer.parseInt(port));
		} catch (NumberFormatException | IOException e1) {
			System.out.println("Error starting server connection: " + e1.getMessage());
		}

		// accept connections for ever
		while (true) {

			try {
				incomingConnection = welcomeSocket.accept();

				// start running accepting connections in background
				Thread run_accept = new Thread(new Accept(incomingConnection));
				run_accept.start();
			} catch (IOException e1) {
				System.out.println("Error in accepting connection: " + e1.getMessage());
			}
		} // end while
	}

	class Accept implements Runnable {
		private Socket connection;

		public Accept(Socket sock) {
			connection = sock;
		}

		@Override
		public void run() {
			BufferedReader readMessage = null;
			DataOutputStream writeMessage = null;
			String line = null;

			int requestNum = -1;
			int serviceNum = -1;

			// assign request number
			synchronized (requestSequence) {
				requestNum = requestSequence.intValue();
				requestSequence += 1;
			}

			// read incoming messages from buffer reader
			try {
				readMessage = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			} catch (IOException e) {
				System.out.println("Error in opening reader buffer for connection: " + e.getMessage());
			}

			// open buffer to write outgoing messages
			try {
				writeMessage = new DataOutputStream(connection.getOutputStream());
			} catch (IOException e) {
				System.out.println("Error in opening writer buffer for connection: " + e.getMessage());
			}

			// read request type and id of incoming connection
			// format "request=read&id=num"
			try {
				line = readMessage.readLine();
			} catch (IOException e) {
				System.out.println("Error reading from connection: " + e.getMessage());
			}

			String tokens[] = line.split("&");

			// get request type
			String request = tokens[0].split("=")[1];

			// get id of incoming connection
			int clientId = Integer.parseInt(tokens[1].split("=")[1]);

			if (request.equals("read")) {
				// handle read request

				Integer newsValue = null;

				try {
					z.acquire();
					rsem.acquire();
					x.acquire();
					readCount++;
					if (readCount == 1) {
						wsem.acquire();
					}
					x.release();
					rsem.release();
					z.release();

					// READ OPERATION
					// copy current value of news board
					newsValue = new Integer(newsBoard.intValue());

					// sleep a bit
					try {
						Thread.sleep(random.nextInt(10) * 1000);
					} catch (InterruptedException e1) {
						System.out.println("Error in sleeping: " + e1.getMessage());
					}
					// END OF READ OPERATION

					x.acquire();
					readCount--;
					if (readCount == 0) {
						wsem.release();
					}
					x.release();
				} catch (InterruptedException e2) {
					System.out.println("Error in one of semaphores: " + e2.getMessage());
				}

				// sync on serviceSequence many are dealing with
				// this
				synchronized (serviceSequence) {
					serviceNum = serviceSequence.intValue();

					// increment service number
					serviceSequence += 1;
				}

				// write a line back to the reader
				// format "rSeq sSeq newsV"
				try {
					writeMessage.writeBytes(requestNum + " " + serviceNum + " " + newsValue.intValue() + "\n");
				} catch (IOException e) {
					System.out.println("Error wirting final message to client: " + e.getMessage());
				}

				// write a log file
				logReader.println(serviceNum + "\t\t " + newsValue + "\t\t " + clientId + "\t\t " + requestNum);
				log.println(serviceNum + "\t\t " + newsValue + "\t\t " + clientId + "\t\t " + requestNum);
			} else if (request.equals("write")) {
				// handle write request

				Integer newsValue = null;

				try {
					y.acquire();
					writeCount++;
					if (writeCount == 1) {
						rsem.acquire();
					}
					y.release();
					wsem.acquire();

					// DO WRITE OPERAION
					// write to newsBoard
					newsBoard = clientId;

					// copy current value of news board
					newsValue = new Integer(newsBoard.intValue());

					// sleep a bit
					try {
						Thread.sleep(random.nextInt(10) * 1000);
					} catch (InterruptedException e1) {
						System.out.println("Error in sleeping: " + e1.getMessage());
					}
					// END OF WRITE OPERATION

					wsem.release();
					y.acquire();
					writeCount--;
					if (writeCount == 0) {
						rsem.release();
					}
					y.release();
				} catch (InterruptedException e2) {
					System.out.println("Error in one of semaphores: " + e2.getMessage());
				}

				// sync on serviceSequence many are dealing with
				// this
				synchronized (serviceSequence) {
					serviceNum = serviceSequence.intValue();

					// increment service number
					serviceSequence += 1;
				}

				// write a line back to the reader
				// format "rSeq sSeq"
				try {
					writeMessage.writeBytes(requestNum + " " + serviceNum + "\n");
				} catch (IOException e) {
					System.out.println("Error wirting final message to client: " + e.getMessage());
				}

				// write a log file
				System.out.println(serviceNum + " " + newsValue + " " + clientId + " " + requestNum);
				logWriter.println(serviceNum + "\t\t" + newsValue + " \t\t" + clientId + "\t\t" + requestNum);
				log.println(serviceNum + "\t\t" + newsValue + " \t\t" + clientId + "\t\t" + requestNum);
			} else {
				// can't happen in scope of this assignment
			}

			// close streams and terminate connection
			try {
				writeMessage.close();
			} catch (IOException e) {
				System.out.println("Error closing output buffer: " + e.getMessage());
			}

			try {
				readMessage.close();
			} catch (IOException e) {
				System.out.println("Error closing input buffer: " + e.getMessage());
			}

			try {
				connection.close();
			} catch (IOException e) {
				System.out.println("Error closing client socket: " + e.getMessage());
			}
		}
	}
}