import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Remote Class for the "Hello, world!" example.
 */
public class NewsBoard extends UnicastRemoteObject implements NewsBoardInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static PrintWriter logReader;
	private static PrintWriter logWriter;
	private static PrintWriter log;

	private static Integer newsBoard = -1;

	private static Integer requestSequence = 1;
	private static Integer serviceSequence = 1;

	// keep a list of all writers ids
	private static Random random = new Random();

	private Semaphore x = new Semaphore(1);
	private Semaphore y = new Semaphore(1);
	private Semaphore z = new Semaphore(1);
	private Semaphore wsem = new Semaphore(1);
	private Semaphore rsem = new Semaphore(1);

	private int writeCount = 0, readCount = 0;

	/**
	 * Construct a remote object
	 * 
	 * @param msg
	 *            the message of the remote object, such as "Hello, world!".
	 * @exception RemoteException
	 *                if the object handle cannot be constructed.
	 */
	public NewsBoard() throws RemoteException {
		super(9092);

		// open a log file
		try {
			logWriter = new PrintWriter(new FileWriter("server-log-writer"), true);
			logReader = new PrintWriter(new FileWriter("server-log-reader"), true);
			log = new PrintWriter(new FileWriter("server-log"), true);
		} catch (IOException e1) {
			System.out.println("Error opening log file: " + e1.getMessage());
		}

		// System.out.println("sSeq oVal ID rNum");
		logWriter.println("Writers:");
		logWriter.println("sSeq\t oVal\t ID\t rNum");

		logReader.println("Readers:");
		logReader.println("sSeq\t oVal\t ID\t rNum");

		log.println("sSeq\t oVal\t ID\t rNum");

		// add ctrl-c catcher
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				logWriter.close();
				logReader.close();
				log.close();
			}
		}));

	}

	@Override
	public String read(int clientId) throws RemoteException {
		Integer newsValue = null;

		int requestNum = -1;
		int serviceNum = -1;

		// assign request number
		synchronized (requestSequence) {
			requestNum = requestSequence.intValue();
			requestSequence += 1;
		}

		try {
			z.acquire();
			rsem.acquire();
			x.acquire();
			readCount++;
			if (readCount == 1) {
				wsem.acquire();
			}
			x.release();
			rsem.release();
			z.release();

			// READ OPERATION
			// copy current value of news board
			newsValue = new Integer(newsBoard.intValue());

			// sleep a bit
			try {
				Thread.sleep(random.nextInt(10) * 1000);
			} catch (InterruptedException e1) {
				System.out.println("Error in sleeping: " + e1.getMessage());
			}
			// END OF READ OPERATION

			x.acquire();
			readCount--;
			if (readCount == 0) {
				wsem.release();
			}
			x.release();
		} catch (InterruptedException e2) {
			System.out.println("Error in one of semaphores: " + e2.getMessage());
		}

		synchronized (serviceSequence) {
			serviceNum = serviceSequence.intValue();

			// increment service number
			serviceSequence += 1;
		}

		// write a line back to the reader
		// format "rSeq sSeq newsV"
		String returned = requestNum + " " + serviceNum + " " + newsValue.intValue();

		// write a log file
		// System.out.println(serviceNum + "\t\t" + newsValue + "\t\t" +
		// clientId + "\t\t"
		// + requestNum);
		logReader
				.println(serviceNum + "\t\t" + newsValue + "\t\t" + clientId + "\t\t" + requestNum);
		log.println(serviceNum + "\t\t" + newsValue + "\t\t" + clientId + "\t\t" + requestNum
				+ "\tr");

		return returned;
	}

	@Override
	public String write(int clientId) throws RemoteException {
		Integer newsValue = null;

		int requestNum = -1;
		int serviceNum = -1;

		// assign request number
		synchronized (requestSequence) {
			requestNum = requestSequence.intValue();
			requestSequence += 1;
		}

		try {
			y.acquire();
			writeCount++;
			if (writeCount == 1) {
				rsem.acquire();
			}
			y.release();
			wsem.acquire();

			// DO WRITE OPERAION
			// write to newsBoard
			newsBoard = clientId;

			// copy current value of news board
			newsValue = new Integer(newsBoard.intValue());

			// sleep a bit
			try {
				Thread.sleep(random.nextInt(10) * 1000);
			} catch (InterruptedException e1) {
				System.out.println("Error in sleeping: " + e1.getMessage());
			}
			// END OF WRITE OPERATION

			wsem.release();
			y.acquire();
			writeCount--;
			if (writeCount == 0) {
				rsem.release();
			}
			y.release();
		} catch (InterruptedException e2) {
			System.out.println("Error in one of semaphores: " + e2.getMessage());
		}
		synchronized (serviceSequence) {
			serviceNum = serviceSequence.intValue();

			// increment service number
			serviceSequence += 1;
		}

		// write a line back to the writer
		// format "rSeq sSeq"
		String returned = requestNum + " " + serviceNum;

		// write a log file
		// System.out.println(serviceNum + " " + newsValue + " " + clientId +
		// " " + requestNum);
		logWriter
				.println(serviceNum + "\t\t" + newsValue + "\t\t" + clientId + "\t\t" + requestNum);
		log.println(serviceNum + "\t\t" + newsValue + "\t\t" + clientId + "\t\t" + requestNum
				+ "\tw");

		return returned;
	}
}