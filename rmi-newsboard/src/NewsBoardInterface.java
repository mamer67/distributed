import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NewsBoardInterface extends Remote {

	/**
	 * Remotely invokable method.
	 * 
	 * @return the message of the remote object, such as "Hello, world!".
	 * @exception RemoteException
	 *                if the remote invocation fails.
	 */
	public String read(int id) throws RemoteException;

	public String write(int id) throws RemoteException;
}
