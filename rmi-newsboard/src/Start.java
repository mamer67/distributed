import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Start {

	private static String host;
	private static int port;

	private static PrintWriter logServer, logReader, logWriter;

	public static void main(String[] args) {
		new Start().run();
	}

	public void run() {
		Parser parser = new Parser();

		// read server attributes
		host = parser.getServerName();
		port = parser.getPortNumber();

		try {
			logServer = new PrintWriter(new FileWriter("start-server"), true);
			logWriter = new PrintWriter(new FileWriter("start-writer"), true);
			logReader = new PrintWriter(new FileWriter("start-reader"), true);
		} catch (IOException e1) {
			System.out.println("Error opening log file: " + e1.getMessage());
		}

		Thread t = new Thread(new Upload(host, port, true, true, host, -1, -1, -1, 0));
		t.start();

		int id = 1;
		int nA = parser.getNumberOfAccess();

		String[] readers = parser.getReaders();
		for (int i = 0; i < readers.length; i++) {
			t = new Thread(new Upload(readers[i], -1, false, true, host, port, id, nA, 200
					* readers.length - i * 200));
			id++;
			t.start();
		}

		String[] writers = parser.getWriters();
		for (int i = 0; i < writers.length; i++) {
			t = new Thread(new Upload(writers[i], -1, false, false, host, port, id, nA, 200
					* readers.length - i * 200 - 100));
			id++;
			t.start();
		}
	}

	class Upload implements Runnable {
		private String h;
		private int prt;

		private String remoteHost;
		private int remotePort;
		private int id;
		private int numberAccess;

		private String fileName = "";
		private boolean server, reader;

		public Upload(String host, int port, boolean server, boolean reader, String remoteHost,
				int remotePort, int id, int numberAccess, int sleep) {
			h = new String(host);
			prt = port;
			fileName = "Server";
			this.server = server;
			this.reader = reader;
			this.remoteHost = remoteHost.split("/")[0].split("@")[1];

//			Random r = new Random();
			if (!server) {
				try {
					if (reader)
						Thread.sleep(sleep + 2000);
				} catch (InterruptedException e) {
					System.out.println("Error in sleeping: " + e.getMessage());
				}

				this.remotePort = remotePort;
				this.id = id;
				this.numberAccess = numberAccess;

				if (reader) {
					fileName = "ReaderClient";
				} else {
					fileName = "WriterClient";
				}
			}
		}

		@Override
		public void run() {

			String[] tokens = h.split("/");
			String[] tokenPermission = tokens[1].split(";");

			Runtime r = Runtime.getRuntime();

			String prefix = "";
			String prefix2 = "";

			if (tokenPermission[0].equals("p")) {
				prefix = "sshpass -p " + tokenPermission[1] + " scp";
				prefix2 = "sshpass -p " + tokenPermission[1] + " ssh";
			} else {
				prefix = "scp -i " + tokenPermission[1];
				prefix2 = "ssh -i " + tokenPermission[1];
			}

			String files = "";

			if (server) {
				files = " src/%s src/%s src/%s ";
				files = String.format(files, "Server.java", "NewsBoardInterface.java",
						"NewsBoard.java");
			} else {
				files = " src/%s src/%s ";
				files = String.format(files, fileName + ".java", "NewsBoardInterface.java");
			}

			// start process
			Process p = null;
			String line = "";
			try {
				String path = "";

				if (!server) {

					if (reader)
						path = "fr" + id;
					else
						path = "fw" + id;

					line = prefix2 + " -p 22 " + tokens[0] + " mkdir " + path;

					p = r.exec(line);
					p.waitFor();
					path += "/";

					// System.out.println(line);
				} else {
					path = "./";
				}

				line = prefix + files + tokens[0] + ":~/" + path;
				p = r.exec(line);
				p.waitFor();
				// System.out.println(line);

				line = prefix2 + " -p 22 " + tokens[0] + " javac -classpath " + path + " " + path
						+ fileName + ".java";
				p = r.exec(line);
				p.waitFor();
//				System.out.println(line);

				if (server) {
					line = prefix2 + " -p 22 " + tokens[0] + " java -classpath " + path + " "
							+ fileName + " " + remoteHost + " " + prt;
					p = r.exec(line);
//					System.out.println(line);
				} else {
					line = prefix2 + " -p 22 " + tokens[0] + " java -classpath " + path + " "
							+ fileName + " " + remoteHost + " " + remotePort + " " + id + " "
							+ numberAccess;
					p = r.exec(line);
//					System.out.println(line);
				}

				System.out.println("connected to: " + tokens[0]);

				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while (true) {
					if (br.ready()) {
						String ll = br.readLine();
						System.out.println(tokens[0] + ": " + ll);
						if (server) {
							logServer.println(ll);
						} else {
							if (reader) {
								logReader.println(ll);
							} else {
								logWriter.println(ll);
							}
						}
					}
				}
			} catch (Exception e) {
				System.out.println("Error executing cmd: " + e.getMessage());
			}
		}
	}
}
