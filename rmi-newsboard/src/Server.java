import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

	/**
	 * Server program for the "Hello, world!" example.
	 * 
	 * @param argv
	 *            The command line arguments which are ignored.
	 */
	public static void main(String[] argv) {
		try {

			 String host = argv[0];
			 String port = argv[1];

			// String host = "localhost";// "whitechimera.no-ip.biz"
			// String port = "9090";

			System.setProperty("java.rmi.server.hostname", host);

			Registry reg = LocateRegistry.createRegistry(Integer.parseInt(port));

			reg.bind("NewsBoard", new NewsBoard());

			System.out.println("NewsBoard online");
		} catch (Exception e) {
			System.out.println("Server failed: " + e);
		}
	}
}
