import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class WriterClient {
	public static void main(String argv[]) throws RemoteException, MalformedURLException,
			NotBoundException {
//		String host = "localhost";
//		String port = "9090";
//		String id = "2";
//		String naccess = "3";

		String host = argv[0];
		String port = argv[1];
		String id = argv[2];
		String naccess = argv[3];

		Registry reg = LocateRegistry.getRegistry(host, Integer.parseInt(port));

		int requestNum = -1;
		int serviceNum = -1;

		System.out.println("rSeq sSeq");
		PrintWriter log = null;

		try {
			log = new PrintWriter(new FileWriter("log-client-w-" + id), true);
		} catch (IOException e1) {
			System.out.println("Error opening log file: " + e1.getMessage());
		}

		log.println("Client type: Writer");
		log.println("Client name: " + id);
		log.println("rSeq\t sSeq");

		int numberAccess = Integer.parseInt(naccess);

		NewsBoardInterface newsboard = (NewsBoardInterface) reg.lookup("NewsBoard");

		for (int i = 0; i < numberAccess; i++) {
			// call the read method in the hello class
			String returned = newsboard.write(Integer.parseInt(id));

			// expected to be "rSeq sSeq"
			String tokens[] = returned.split(" ");
			requestNum = Integer.parseInt(tokens[0]);
			serviceNum = Integer.parseInt(tokens[1]);

			// write to log
			System.out.println(requestNum + " " + serviceNum);
			log.println(requestNum + "\t\t " + serviceNum);

		}
		log.close();
	}
}