import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ReaderClient {
	public static void main(String argv[]) throws RemoteException, MalformedURLException,
			NotBoundException {
//		String host = "localhost";
//		String port = "9090";
//		String id = "1";
//		String naccess = "3";

		String host = argv[0];
		String port = argv[1];
		String id = argv[2];
		String naccess = argv[3];

		Registry reg = LocateRegistry.getRegistry(host, Integer.parseInt(port));

		int requestNum = -1;
		int serviceNum = -1;
		int newsValue = -2;

		System.out.println("rSeq sSeq oVal");
		PrintWriter log = null;

		try {
			log = new PrintWriter(new FileWriter("log-client-r-" + id), true);
		} catch (IOException e1) {
			System.out.println("Error opening log file: " + e1.getMessage());
		}

		log.println("Client type: Reader");
		log.println("Client name: " + id);
		log.println("rSeq\t sSeq\t oVal");

		int numberAccess = Integer.parseInt(naccess);

		NewsBoardInterface newsboard = (NewsBoardInterface) reg.lookup("NewsBoard");

		for (int i = 0; i < numberAccess; i++) {
			// call the read method in the hello class
			String returned = newsboard.read(Integer.parseInt(id));

			// expected to be "requestNumber serviceNumber newsVAlue"
			String tokens[] = returned.split(" ");
			requestNum = Integer.parseInt(tokens[0]);
			serviceNum = Integer.parseInt(tokens[1]);
			newsValue = Integer.parseInt(tokens[2]);

			// write to log
			System.out.println(requestNum + " " + serviceNum + " " + newsValue);
			log.println(requestNum + "\t\t " + serviceNum + "\t\t " + newsValue);

		}

		log.close();
	}
}