import java.io.Serializable;

/**
 * Works as holder for the data that need to be written
 * @author aamer
 */
public class FileContent implements Serializable {

	/**/
	private static final long serialVersionUID = 1L;

	private long timestamp;
	private String fileName;
	private byte[] data;

	public FileContent(String fn) {
		fileName = fn;
	}

	public FileContent(String fn, byte[] d) {
		fileName = fn;
		data = d;
	}

	public FileContent(String fn, byte[] d, long ts) {
		fileName = fn;
		data = d;
		timestamp = ts;
	}

	public void setTimestamp(long ts) {
		timestamp = ts;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public byte[] getData() {
		return data;
	}

	public String getFileName() {
		return fileName;
	}
}
