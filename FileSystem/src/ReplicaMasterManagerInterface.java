import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * This interface is used by master to communicate with replica servers
 * @author aamer
 */
public interface ReplicaMasterManagerInterface extends Remote {

	/* tests if server is alive or dead */
	public boolean bingReplica() throws RemoteException ;

	/* get a list of the file names on the replica */
	public String[] getFileNames() throws RemoteException ;

	/* set the replicas for the primary server */
	public void setReplicasToPrimary(Transaction trx, ArrayList<ReplicaLoc> rpls, boolean newFile,
			String host, int port) throws RemoteException ;
}
