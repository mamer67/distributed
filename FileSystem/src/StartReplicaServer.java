import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class StartReplicaServer {

	/**
	 * args[0] should be host name of master server
	 * args[1] should be port number of registry
	 * args[2] should be port number of replica server object
	 * args[3] path of the directory
	 * args[4] start from crash or start over
	 * @param args
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static void main(String[] args) throws NumberFormatException, IOException {
		if(args.length < 5){
			
			System.out.println("Usage: java -jar  replica.jar host registerportnumber objectportnumber dirfolder false");
			return;
		}
		
		System.setProperty("java.rmi.server.hostname", args[0]);

		// create registry
		Registry reg = LocateRegistry.createRegistry(Integer.parseInt(args[1]));

		// create and bind object
		ReplicaServer master = new ReplicaServer(Integer.parseInt(args[2]), args[3],
				Boolean.parseBoolean(args[4]));
		reg.rebind(FileSystem.REPLICAOBJECTNAME, master);

		// create and bind master manager object
		System.out.println("Replica server started");
	}
}
