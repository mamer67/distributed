import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class ReplicaLog {

	private PrintWriter replicaLog;
	private PrintWriter replicaCrashLog;

	private String replicaLogName = "replicalog";
	private String replicaCrashLogName = "replicacrashlog";

	/**
	 * open new logs, appending to last log if exist, but creating new crash log
	 */
	public ReplicaLog(boolean resume, int port) {
		replicaLogName += port;
		replicaCrashLogName += port;
		
		try {
			replicaLog = new PrintWriter(new FileWriter(replicaLogName, true), true);

			if (!resume)
				replicaCrashLog = new PrintWriter(new FileWriter(replicaCrashLogName), true);

		} catch (Exception e) {
			System.out.println("Error opening file in ReplicaLog");
		}
	}

	public String getCrashLog() {
		return replicaCrashLogName;
	}

	public void initializeCrashLog() {
		try {
			replicaCrashLog = new PrintWriter(new FileWriter(replicaCrashLogName), true);
		} catch (Exception e) {
			System.out.println("Error opening file in ReplicaLog");
		}
	}

	public void writeRecord(ReplicaLogRecord r) {
		replicaLog.println(r.toString());
		replicaCrashLog.println(r.toString());
	}

	public void close() {
		replicaLog.close();
		replicaCrashLog.close();

		// delete crash log
		File f = new File(replicaCrashLogName);
		f.delete();
	}
}
