import java.util.ArrayList;

/**
 * This class is used to be as a history tuple for replicaServer
 * @author aamer
 *
 */
public class Tuple {

	private Transaction transaction;
	private ArrayList<ReplicaLoc> replicas;
	private boolean newFile;
	private boolean primary;
	private ArrayList<WriteTransaction> wTransList = new ArrayList<WriteTransaction>();
	private String host;
	private int port;

	private boolean running;

	public Tuple(Transaction trx, ArrayList<ReplicaLoc> rpl, boolean nfile, boolean pri) {
		transaction = trx;
		replicas = rpl;
		newFile = nfile;
		primary = pri;

		running = false;
	}

	public Tuple(Transaction trx, ArrayList<ReplicaLoc> rpl, boolean nfile, boolean pri,
			String hst, int prt) {
		transaction = trx;
		replicas = rpl;
		newFile = nfile;
		primary = pri;

		host = hst;
		port = prt;

		running = false;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean run) {
		running = run;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public boolean isPrimary() {
		return primary;
	}

	public String getFileName() {
		return transaction.getFileName();
	}

	public ArrayList<ReplicaLoc> getReplicas() {
		return replicas;
	}

	public int getMsgNo() {
		return wTransList.size();
	}

	public ArrayList<WriteTransaction> getMessages() {
		return wTransList;
	}

	public boolean isNewFile() {
		return newFile;
	}

	public boolean addWriteTransaction(WriteTransaction wt) {
		// get last write transaction
		if (wTransList.size() > 0) {
			WriteTransaction prev = wTransList.get(wTransList.size() - 1);

			if (wt.getSeqno() - prev.getSeqno() != 1)
				return false;
		}

		// correct seq no, so add message
		wTransList.add(wt);

		return true;
	}
}
