import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This Object, will act as a MasterServer which handles the client requests
 * @author aamer
 *
 */
public class MasterServer extends UnicastRemoteObject implements MasterServerClientInterface,
		MasterReplicaMangerInterface {

	private static int transactionCounter = 1;

	private static final long serialVersionUID = 0x1L;

	// save replica addresses in memory
	private ArrayList<ReplicaLoc> replicaServers = new ArrayList<ReplicaLoc>(5);

	// dynamic meta data about files and their locations
	// table to hold file name, and its replica servers ids, which contain this file
	private ConcurrentHashMap<String, ArrayList<ReplicaLoc>> fileTable = new ConcurrentHashMap<String, ArrayList<ReplicaLoc>>(
			100);

	// create master log here
	private MasterLog log;

	// create random to choose replica at random
	private Random random = new Random(255);

	private String host;
	private int port;

	public MasterServer(String host, int registeryport, int port, boolean crash) throws IOException {
		super(port);

		this.host = host;
		this.port = registeryport;

		// start by reading replica locations
		BufferedReader read = new BufferedReader(new FileReader("repServers.txt"));
		String line = "";

		int num = 1;
		while (read.ready()) {
			line = read.readLine();

			String tokens[] = line.split(":");

			replicaServers.add(new ReplicaLoc(tokens[0], Integer.parseInt(tokens[1]), num));
		}

		read.close();

		// start binging the replicas and build metadata
		bingReplicas(true);

		// TODO consider periodically detecting files on replicas as well
		// run a thread to periodically bing servers
		new Thread(bingReplicas).start();

		// begin crash handling here
		if (crash) {
			// TODO crash handling
		} else {
			log = new MasterLog(false);
		}
	}

	private Runnable bingReplicas = new Runnable() {

		@Override
		public void run() {
			bingReplicas(false);
		}
	};

	// this method bings the replica servers and add its status
	private void bingReplicas(boolean addToMetaData) {

		for (int i = 0; i < replicaServers.size(); i++) {
			ReplicaLoc replicaLocation = replicaServers.get(i);
			Registry reg = null;

			try {
				reg = LocateRegistry.getRegistry(replicaLocation.getHost(),
						replicaLocation.getPort());
			} catch (RemoteException e) {
				// replica is down , set status
				replicaLocation.setStatus(false);
			}

			ReplicaMasterManagerInterface replicaBeat = null;
			try {
				replicaBeat = (ReplicaMasterManagerInterface) reg
						.lookup(FileSystem.REPLICAOBJECTNAME);

				// test heart beat
				if (replicaBeat.bingReplica()) {
					replicaLocation.setStatus(true);

					if (addToMetaData) {
						// add files
						String files[] = replicaBeat.getFileNames();

						// add files to metadata
						for (int k = 0; k < files.length; k++) {
							addFileToMetaData(files[k], replicaLocation);
						}
					}
				} else {
					// unknown state
					replicaLocation.setStatus(false);
				}
			} catch (RemoteException | NotBoundException e) {
				// replica is down , set status
				replicaLocation.setStatus(false);
			}
		}
	}

	private void addFileToMetaData(String filename, ReplicaLoc replica) {
		// First insert into fileTable
		ArrayList<ReplicaLoc> replicas = null;

		if (fileTable.containsKey(filename)) {
			// key found before , return old list of replicas
			replicas = fileTable.get(filename);
		} else {
			// create new list of replicas for the file
			replicas = new ArrayList<ReplicaLoc>(5);

			// add file to filetable
			fileTable.put(filename, replicas);
		}

		// add the replica id
		replicas.add(replica);
	}

	@Override
	public ReplicaLoc[] read(String fileName) throws FileNotFoundException, IOException,
			RemoteException {
		// first create transaction for this
		Transaction trx = new Transaction(transactionCounter++, Transaction.READ, fileName);

		// write to log
		MasterLogRecord recrd = new MasterLogRecord(trx, MasterLogRecord.Request);
		log.writeRecord(recrd);

		// do read operation
		ReplicaLoc[] rpls = doRead(trx);

		// write to log again
		recrd.setTransactionType(MasterLogRecord.ACK);
		log.writeRecord(recrd);

		return rpls;
	}

	private ReplicaLoc[] doRead(Transaction trx) {
		ArrayList<ReplicaLoc> frpls = fileTable.get(trx.getFileName());

		if (frpls == null) {
			// file doesn't exist
			return null;
		} else {
			// get all replica locks for this file
			ArrayList<ReplicaLoc> replicas = new ArrayList<ReplicaLoc>();
			replicas.addAll(frpls);

			for (int i = 0; i < replicas.size(); i++) {
				if (replicas.get(i).getStatus() == false) {
					replicas.remove(i);
					i--;
				}
			}

			ReplicaLoc[] rpls = new ReplicaLoc[replicas.size()];
			replicas.toArray(rpls);

			return rpls;
		}
	}

	@Override
	public WriteMsg write(FileContent data) throws RemoteException, IOException {
		String filename = data.getFileName();

		// first create transaction for this
		Transaction trx = new Transaction(transactionCounter++, Transaction.READ, filename);

		// write to log
		MasterLogRecord recrd = new MasterLogRecord(trx, MasterLogRecord.Request);
		log.writeRecord(recrd);

		// do write operation
		WriteMsg msg = doWrite(trx);

		// write to log again
		recrd.setTransactionType(MasterLogRecord.ACK);
		log.writeRecord(recrd);

		return msg;
	}

	//	private boolean test = true;

	private WriteMsg doWrite(Transaction trx) {
		boolean newFile = false;
		int numOfReplicas, num, last;

		// get replicas of file
		ArrayList<ReplicaLoc> fileReplica = new ArrayList<ReplicaLoc>();

		if (fileTable.get(trx.getFileName()) != null)
			fileReplica.addAll(fileTable.get(trx.getFileName()));

		if (fileReplica.size() <= 0) {
			// file doesn't exist
			newFile = true;
			fileReplica = new ArrayList<ReplicaLoc>();

			// new file is created, choose any 2 replicas online
			for (int i = 0; i < replicaServers.size() && fileReplica.size() < 2; i++) {
				ReplicaLoc loc = replicaServers.get(i);

				if (loc.getStatus()) {
					fileReplica.add(loc);
				}
			}
		}

		// get primary replica here
		ReplicaLoc primary = null;

		numOfReplicas = fileReplica.size();
		num = random.nextInt(numOfReplicas);
		//		test = false;
		last = num;

		// loop until set one replica as primary, one must be online
		do {
			ReplicaLoc rlc = fileReplica.get(num);

			if (rlc.getStatus()) {
				primary = rlc;
				fileReplica.remove(num);
				break;
			}

			num = (num + 1) % numOfReplicas;
		} while (num != last);

		// communicate with primary
		Registry reg = null;

		try {
			reg = LocateRegistry.getRegistry(primary.getHost(), primary.getPort());
		} catch (RemoteException e) {
			//
			System.out.println("Error communicating with primary server");
		}

		ReplicaMasterManagerInterface replicaBeat = null;
		try {
			replicaBeat = (ReplicaMasterManagerInterface) reg.lookup(FileSystem.REPLICAOBJECTNAME);

			replicaBeat.setReplicasToPrimary(trx, fileReplica, newFile, host, port);
		} catch (RemoteException | NotBoundException e) {
			//
			System.out.println("Error communicating with primary server");
		}

		// write message
		WriteMsg msg = new WriteMsg(trx.getId(), trx.getTimestamp(), primary);

		return msg;
	}

	@Override
	public void addFileToMetaData(String filename, ArrayList<ReplicaLoc> replicas)
			throws RemoteException {
		if (fileTable.containsKey(filename) == false)
			fileTable.put(filename, replicas);
		else {
			// file already exist
			System.out
					.println("Warning file already exist, No adding is done. Must be added again when replica started from failure");
		}
	}
}
