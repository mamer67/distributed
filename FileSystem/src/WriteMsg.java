import java.io.Serializable;

/**
 * it acts as a response for the client when client requests write
 * @author aamer
 *
 */
public class WriteMsg implements Serializable {
	/**/
	private static final long serialVersionUID = 1L;

	public static final int WRONGSEQNO = 0x01;
	public static final int SUCCESS = 0x02;

	private long transactionId;
	private long timeStamp;
	private ReplicaLoc loc;

	private int error;

	public WriteMsg(long tid, long ts, ReplicaLoc lc) {
		this.transactionId = tid;
		this.timeStamp = ts;
		this.loc = lc;
	}

	public WriteMsg(long tid, long ts) {
		this.transactionId = tid;
		this.timeStamp = ts;
	}

	public void setMsg(int error) {
		this.error = error;
	}

	public int getMsg() {
		return error;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public ReplicaLoc getLoc() {
		return loc;
	}
}