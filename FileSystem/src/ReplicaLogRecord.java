import javax.xml.bind.DatatypeConverter;

/**
 * This class carries a master lock record
 * @author aamer
 *
 */
public class ReplicaLogRecord {
	public static final int Request = 0x01;
	public static final int ACK = 0x02;

	private Transaction transaction;
	private int transactionType;

	private long seqNo;
	private byte[] data;

	public ReplicaLogRecord(Transaction t, int transactiontype) {
		transaction = t;
		transactionType = transactiontype;
	}

	public ReplicaLogRecord(Transaction t, int transactiontype, long seqno, byte[] data) {
		transaction = t;
		transactionType = transactiontype;

		this.seqNo = seqno;
		this.data = data;
	}

	public int getLockType() {
		return transactionType;
	}

	public void setTransactionType(int ttype) {
		transactionType = ttype;
	}

	public String toString() {

		if (transaction.getType() == Transaction.READ || transaction.getType() == Transaction.ABORT
				|| transactionType == ACK)
			return String.format("%20d %5d %20s %3d %3d", transaction.getTimestamp(),
					transaction.getId(), transaction.getFileName(), transaction.getType(),
					transactionType);
		else
			return String.format("%20d %5d %20s %3d %3d %5d %s", transaction.getTimestamp(),
					transaction.getId(), transaction.getFileName(), transaction.getType(),
					transactionType, seqNo, DatatypeConverter.printHexBinary(data));

		//DatatypeConverter.parseHexBinary
	}
}
