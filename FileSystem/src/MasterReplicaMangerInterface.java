import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * This interface to help replica communicating with the Master
 * @author aamer
 *
 */
public interface MasterReplicaMangerInterface extends Remote {

	public void addFileToMetaData(String filename, ArrayList<ReplicaLoc> replicas) throws RemoteException ;
}
