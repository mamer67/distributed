import java.util.Hashtable;

/**
 * This class acts as a lock manager, and it follows time stamp reordering
 * also this class it is used by replicas only
 * 
 * @author aamer
 *
 */
public class FileLockTest {

	/* main to test the lock manager */
	public static void main(String[] args) {
		final Transaction t1 = new Transaction(1, Transaction.READ, "filex.txt");
		final Transaction t2 = new Transaction(2, Transaction.WRITE, "filey.txt");
		final Transaction t3 = new Transaction(3, Transaction.READ, "filex.txt");

		final FileLockTest lm = new FileLockTest();
		lm.addFileLock(new FileLock("filex.txt"));
		lm.addFileLock(new FileLock("filey.txt"));

		lm.acquireLock(t3);
		System.out.println("t3 started");

		new Thread(new Runnable() {

			@Override
			public void run() {
				lm.acquireLock(t2);
				System.out.println("t2 started");
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				lm.acquireLock(t1);
				System.out.println("t1 started");
			}
		}).start();
	}

	/* No need to keep history of locks, as replicas will use this LockManager, so they know 
	 * how to handle locks
	 */
	private Hashtable<String, FileLock> files = new Hashtable<String, FileLock>(100);

	/**
	 * adds file lock to lock manager
	 * @param fl
	 */
	public void addFileLock(FileLock fl) {
		files.put(fl.getFileName(), fl);
	}

	/**
	 * Call this method and acquire lock
	 */
	public void acquireLock(Transaction t) {
		FileLock fl = files.get(t.getFileName());

		if (t.getType() == Transaction.READ) {
			fl.AcquireReadLock();
		} else {
			fl.AcquireWriteLock();
		}
	}

	public void releaseLock(Transaction t) {
		FileLock fl = files.get(t.getFileName());

		if (t.getType() == Transaction.READ) {
			fl.ReleaseReadLock();
		} else {
			fl.ReleaseWriteLock();
		}
	}
}
