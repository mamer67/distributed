import java.io.Serializable;

/**
 * The class carries transaction id, and type
 * 
 * @author aamer
 */
public class Transaction implements Comparable<Transaction>, Serializable {

	/**/
	private static final long serialVersionUID = 1L;

	public static final int READ = 0x1;
	public static final int WRITE = 0x2;
	public static final int COMMIT = 0x03;
	public static final int ABORT = 0x04;

	private long id;
	private int type;
	private long timestamp;
	private String filename;

	public Transaction(long id, int type, String filename) {
		this.id = id;
		this.type = type;
		this.filename = filename;

		timestamp = System.nanoTime();

	}

	public String getFileName() {
		return filename;
	}

	public void setFileName(String name) {
		this.filename = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String toString() {
		return String.format("%d %d %s", timestamp, id, type == READ ? "Read" : "Write");
	}

	@Override
	public int compareTo(Transaction t) {
		if (timestamp < t.timestamp)
			return -1;
		else if (timestamp > t.timestamp)
			return 1;
		else
			return 0;
	}
}
