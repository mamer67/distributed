import java.util.concurrent.Semaphore;

/**
 * This class handles FileLocks, to be used by LockManager
 * Writers have priority 
 * 
 * @author aamer
 *
 */
public class FileLock {

	private String fileName;

	/* Semaphores to keep concurrency between files */
	private Semaphore x = new Semaphore(1);
	private Semaphore y = new Semaphore(1);
	private Semaphore z = new Semaphore(1);
	private Semaphore wsem = new Semaphore(1);
	private Semaphore rsem = new Semaphore(1);
	private int writeCount = 0, readCount = 0;

	public FileLock(String fn) {
		fileName = fn;
	}

	public String getFileName() {
		return fileName;
	}

	public void AcquireReadLock() {
		try {
			z.acquire();
			rsem.acquire();
			x.acquire();
			readCount++;
			if (readCount == 1) {
				wsem.acquire();
			}
			x.release();
			rsem.release();
			z.release();

			// DO READ OPERATION
		} catch (InterruptedException e2) {
			System.out.println("Error in one of semaphores: " + e2.getMessage());
		}
	}

	public void ReleaseReadLock() {
		try {
			x.acquire();
			readCount--;
			if (readCount == 0) {
				wsem.release();
			}
			x.release();
		} catch (InterruptedException e2) {
			System.out.println("Error in one of semaphores: " + e2.getMessage());
		}
	}

	public void AcquireWriteLock() {
		try {
			y.acquire();
			writeCount++;
			if (writeCount == 1) {
				rsem.acquire();
			}
			y.release();
			wsem.acquire();

			// DO WRITE OPERAION
		} catch (InterruptedException e2) {
			System.out.println("Error in one of semaphores: " + e2.getMessage());
		}
	}

	public void ReleaseWriteLock() {
		try {
			wsem.release();
			y.acquire();
			writeCount--;
			if (writeCount == 0) {
				rsem.release();
			}
			y.release();
		} catch (InterruptedException e2) {
			System.out.println("Error in one of semaphores: " + e2.getMessage());
		}
	}
}
