
/**
 * This class is used to hold the write transaction, so it should have seqno, and file content
 * @author aamer
 *
 */
public class WriteTransaction {
	private long seqno;
	private FileContent fc;

	public WriteTransaction(long sq, FileContent f) {
		seqno = sq;
		fc = f;
	}

	public long getSeqno() {
		return seqno;
	}

	public FileContent getFc() {
		return fc;
	}

}
