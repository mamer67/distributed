public interface FileSystem {

	/* name of the master object when it binds to the registry */
	public static final String MASTEROBJECTNAME = "masterserverobject";

	/* name of the replica object when it binds to the registry */
	public static final String REPLICAOBJECTNAME = "replicaserverobject";
	
	/* name of actions supported by master, either client request, or master acks request */
	public static final String REQUEST = "Request";
	public static final String ACK = "ACK";
}
