import java.io.Serializable;

public class ReplicaLoc implements Serializable {

	/**/
	private static final long serialVersionUID = 1L;

	private int id;
	private String host;
	private int port;

	private boolean status;

	public ReplicaLoc(String h, int p, int i) {
		host = h;
		port = p;
		id = i;
	}

	public int getId() {
		return id;
	}

	public void setId(int i) {
		this.id = i;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean s) {
		this.status = s;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String toString() {
		return String.format("h=%s p=%d s=%s", host, port, status == true ? "life" : "dead");
	}
}
