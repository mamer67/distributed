import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.junit.Test;

public class ClientTest {

	@Test
	public void test() throws NumberFormatException, IOException, NotBoundException,
			MessageNotFoundException, InterruptedException {

		//		StartReplicaServer.main(new String[] { "whitechimera.no-ip.biz", "9092", "9093", "data1",
		//				"false" });
		//		StartReplicaServer.main(new String[] { "whitechimera.no-ip.biz", "9094", "9095", "data2",
		//				"false" });
		//
		//		StartMasterServer.main(new String[] { "whitechimera.no-ip.biz", "9090", "9091", "false" });

		test12();
		//		test2();
		//		test3();
		//		test4();
		//		test5();
		//		test6();
		//		test7();
		//		test8();
		//		test9();
		//		test10();
		//		test11();
	}

	/**
	 * Test reads and writes on different files
	 * 
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	private Registry reg;
	private MasterServerClientInterface masterServer;

	public void test12() throws NotBoundException, FileNotFoundException, IOException {
		String hostname = "foundit2.cloudapp.net";

		reg = LocateRegistry.getRegistry(hostname, 9090);
		masterServer = (MasterServerClientInterface) reg.lookup(FileSystem.MASTEROBJECTNAME);

		done = 0;
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					String filename = "filex.txt";

					ReplicaLoc[] replicas = masterServer.read(filename);

					reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

					ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
							.lookup(FileSystem.REPLICAOBJECTNAME);

					FileContent data = replicaServer.read(filename);

					System.out.println(new String(data.getData()));

					synchronized (done) {
						done++;
					}
				} catch (Exception e) {

				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					String filename = "filey.txt";

					ReplicaLoc[] replicas = masterServer.read(filename);

					reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

					ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
							.lookup(FileSystem.REPLICAOBJECTNAME);

					FileContent data = replicaServer.read(filename);

					System.out.println(new String(data.getData()));

					synchronized (done) {
						done++;
					}
				} catch (Exception e) {

				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					String filename = "filex.txt";
					WriteMsg msg = masterServer.write(new FileContent(filename));

					ReplicaLoc loc = msg.getLoc();

					reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

					ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
							.lookup(FileSystem.REPLICAOBJECTNAME);

					WriteMsg response = replicaServer.write(msg.getTransactionId(), 0,
							new FileContent(filename, "msgnox1".getBytes(), msg.getTimeStamp()));

					System.out.println(response.getMsg() == WriteMsg.SUCCESS);

					System.out.println(replicaServer.commit(msg.getTransactionId(), 1));

					synchronized (done) {
						done++;
					}
				} catch (Exception e) {

				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					String filename = "filex.txt";
					WriteMsg msg = masterServer.write(new FileContent(filename));

					ReplicaLoc loc = msg.getLoc();

					reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

					ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
							.lookup(FileSystem.REPLICAOBJECTNAME);

					WriteMsg response = replicaServer.write(msg.getTransactionId(), 0,
							new FileContent(filename, "msgnox2".getBytes(), msg.getTimeStamp()));

					System.out.println(response.getMsg() == WriteMsg.SUCCESS);

					System.out.println(replicaServer.commit(msg.getTransactionId(), 1));

					synchronized (done) {
						done++;
					}
				} catch (Exception e) {

				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					String filename = "filey.txt";
					WriteMsg msg = masterServer.write(new FileContent(filename));

					ReplicaLoc loc = msg.getLoc();

					reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

					ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
							.lookup(FileSystem.REPLICAOBJECTNAME);

					WriteMsg response = replicaServer.write(msg.getTransactionId(), 0,
							new FileContent(filename, "msgnoy1".getBytes(), msg.getTimeStamp()));

					System.out.println(response.getMsg() == WriteMsg.SUCCESS);

					System.out.println(replicaServer.commit(msg.getTransactionId(), 1));

					synchronized (done) {
						done++;
					}
				} catch (Exception e) {

				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					String filename = "filey.txt";
					WriteMsg msg = masterServer.write(new FileContent(filename));

					ReplicaLoc loc = msg.getLoc();

					reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

					ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
							.lookup(FileSystem.REPLICAOBJECTNAME);

					WriteMsg response = replicaServer.write(msg.getTransactionId(), 0,
							new FileContent(filename, "msgnoy2".getBytes(), msg.getTimeStamp()));

					System.out.println(response.getMsg() == WriteMsg.SUCCESS);

					System.out.println(replicaServer.commit(msg.getTransactionId(), 1));

					synchronized (done) {
						done++;
					}
				} catch (Exception e) {

				}
			}
		}).start();
		
		while (true) {
			synchronized (done) {
				if (done == 6)
					break;
			}
		}
	}

	/**
	 * Test Normal Read
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void test1() throws NotBoundException, FileNotFoundException, IOException {

		String hostname = "foundit2.cloudapp.net";
		String filename = "filexyzx.txt";

		Registry reg = LocateRegistry.getRegistry(hostname, 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		ReplicaLoc[] replicas = masterServer.read(filename);

		reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		FileContent data = replicaServer.read(filename);

		System.out.println(new String(data.getData()));
	}

	/**
	 * Test Normal Write
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 */
	public void test2() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException {

		Registry reg = LocateRegistry.getRegistry("whitechimera.no-ip.biz", 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		WriteMsg msg = masterServer.write(new FileContent("filex.txt"));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				"filex.txt", "msgno1".getBytes(), msg.getTimeStamp()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 4, new FileContent("filex.txt",
				"msgno4".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1, new FileContent("filex.txt",
				"new2".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 2, new FileContent("filex.txt",
				"new3".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		System.out.println(replicaServer.commit(msg.getTransactionId(), 4));

		System.out.println(replicaServer.commit(msg.getTransactionId(), 3));
	}

	/**
	 * Test Write to a new file
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 */
	public void test3() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException {

		Registry reg = LocateRegistry.getRegistry("whitechimera.no-ip.biz", 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		WriteMsg msg = masterServer.write(new FileContent("filez.txt"));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				"filez.txt", "msgno1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 4, new FileContent("filez.txt",
				"msgno4".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1, new FileContent("filez.txt",
				"new2".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 2, new FileContent("filez.txt",
				"new3".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		System.out.println(replicaServer.commit(msg.getTransactionId(), 4));

		System.out.println(replicaServer.commit(msg.getTransactionId(), 3));
	}

	/**
	 * Test consistency on existed file
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 * @throws InterruptedException 
	 */
	private Integer done = 0;

	public void test4() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException, InterruptedException {

		String hostname = "foundit2.cloudapp.net";
		String filename = "filex.txt";

		Registry reg = LocateRegistry.getRegistry(hostname, 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		final WriteMsg msg = masterServer.write(new FileContent(filename));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		final ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				filename, "commit1".getBytes(), msg.getTimeStamp()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1, new FileContent(filename,
				"-commit1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		Thread.sleep(1000);

		final WriteMsg msg2 = masterServer.write(new FileContent(filename));

		ReplicaLoc loc2 = msg2.getLoc();

		reg = LocateRegistry.getRegistry(loc2.getHost(), loc2.getPort());

		final ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response2 = replicaServer2.write(msg2.getTransactionId(), 0, new FileContent(
				filename, "commit2".getBytes(), msg2.getTimeStamp()));

		System.out.println(response2.getMsg() == WriteMsg.SUCCESS);

		response2 = replicaServer2.write(msg2.getTransactionId(), 1, new FileContent(filename,
				"-commit2".getBytes()));

		System.out.println(response2.getMsg() == WriteMsg.SUCCESS);

		System.out.println(replicaServer.toString());
		System.out.println(replicaServer2.toString());

		done = 0;
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println(replicaServer2.commit(msg2.getTransactionId(), 2));
					System.out.println("finish 2");
				} catch (Exception e) {
					e.printStackTrace();
				}
				synchronized (done) {
					done++;
				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println(replicaServer.commit(msg.getTransactionId(), 2));
					System.out.println("finish 1");
				} catch (Exception e) {
					e.printStackTrace();
				}
				synchronized (done) {
					done++;
				}
			}
		}).start();

		while (true) {
			synchronized (done) {
				if (done == 2)
					break;
			}
		}
	}

	/**
	 * Test consistency on none existed file
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 * @throws InterruptedException 
	 */

	public void test5() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException, InterruptedException {

		String filename = "filexyz.txt";
		String hostname = "foundit2.cloudapp.net";

		Registry reg = LocateRegistry.getRegistry(hostname, 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		final WriteMsg msg = masterServer.write(new FileContent(filename));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		final ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				filename, "commit1".getBytes(), msg.getTimeStamp()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1, new FileContent(filename,
				"-commit1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		Thread.sleep(1000);

		final WriteMsg msg2 = masterServer.write(new FileContent(filename));

		ReplicaLoc loc2 = msg2.getLoc();

		reg = LocateRegistry.getRegistry(loc2.getHost(), loc2.getPort());

		final ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response2 = replicaServer2.write(msg2.getTransactionId(), 0, new FileContent(
				filename, "commit2".getBytes(), msg2.getTimeStamp()));

		System.out.println(response2.getMsg() == WriteMsg.SUCCESS);

		response2 = replicaServer2.write(msg2.getTransactionId(), 1, new FileContent(filename,
				"-commit2".getBytes()));

		System.out.println(response2.getMsg() == WriteMsg.SUCCESS);

		System.out.println(replicaServer.toString());
		System.out.println(replicaServer2.toString());

		done = 0;
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println(replicaServer2.commit(msg2.getTransactionId(), 2));
					System.out.println("finish 2");
				} catch (Exception e) {
					e.printStackTrace();
				}
				synchronized (done) {
					done++;
				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println(replicaServer.commit(msg.getTransactionId(), 2));
					System.out.println("finish 1");
				} catch (Exception e) {
					e.printStackTrace();
				}
				synchronized (done) {
					done++;
				}
			}
		}).start();

		while (true) {
			synchronized (done) {
				if (done == 2)
					break;
			}
		}
	}

	/**
	 * Test Partial updates are not seen by other trx
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 */
	public void test6() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException {

		String filename = "filex.txt";
		Registry reg = LocateRegistry.getRegistry("whitechimera.no-ip.biz", 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		WriteMsg msg = masterServer.write(new FileContent(filename));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				filename, "msgno1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1,
				new FileContent(filename, "new2".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 2,
				new FileContent(filename, "new3".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		// do read here 
		ReplicaLoc[] replicas = masterServer.read(filename);

		reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

		ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		FileContent data = replicaServer2.read(filename);

		System.out.println(new String(data.getData()));

		System.out.println(replicaServer.commit(msg.getTransactionId(), 3));

		// do read again 
		replicas = masterServer.read(filename);

		reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

		replicaServer2 = (ReplicaServerClientInterface) reg.lookup(FileSystem.REPLICAOBJECTNAME);

		data = replicaServer2.read(filename);

		System.out.println(new String(data.getData()));
	}

	/**
	 * new file visible only when commit
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 */
	public void test7() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException {

		String filename = "filexyz.txt";
		Registry reg = LocateRegistry.getRegistry("whitechimera.no-ip.biz", 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		WriteMsg msg = masterServer.write(new FileContent(filename));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				filename, "msgno1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1,
				new FileContent(filename, "new2".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 2,
				new FileContent(filename, "new3".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		// do read here 
		ReplicaLoc[] replicas = masterServer.read(filename);

		if (replicas != null) {
			reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

			ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
					.lookup(FileSystem.REPLICAOBJECTNAME);

			FileContent data = replicaServer2.read(filename);

			System.out.println(new String(data.getData()));
		} else {
			System.out.println("File doesn't exist");
		}

		System.out.println(replicaServer.commit(msg.getTransactionId(), 3));

		// do read again 
		replicas = masterServer.read(filename);

		reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

		ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		FileContent data = replicaServer2.read(filename);

		System.out.println(new String(data.getData()));
	}

	/**
	 * After abort no updates are done to file
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 */
	public void test8() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException {

		String filename = "filex.txt";
		Registry reg = LocateRegistry.getRegistry("whitechimera.no-ip.biz", 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		WriteMsg msg = masterServer.write(new FileContent(filename));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				filename, "msgno1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1,
				new FileContent(filename, "new2".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 2,
				new FileContent(filename, "new3".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		// do read here 
		ReplicaLoc[] replicas = masterServer.read(filename);

		reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

		ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		FileContent data = replicaServer2.read(filename);

		System.out.println(new String(data.getData()));

		System.out.println(replicaServer.abort(msg.getTransactionId()));
		System.out.println(replicaServer.commit(msg.getTransactionId(), 3));

		// do read again 
		replicas = masterServer.read(filename);

		reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

		replicaServer2 = (ReplicaServerClientInterface) reg.lookup(FileSystem.REPLICAOBJECTNAME);

		data = replicaServer2.read(filename);

		System.out.println(new String(data.getData()));
	}

	/**
	 * new file visible only when commit, not if aborted
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 */
	public void test9() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException {

		String filename = "filexyz.txt";
		Registry reg = LocateRegistry.getRegistry("whitechimera.no-ip.biz", 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		WriteMsg msg = masterServer.write(new FileContent(filename));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				filename, "msgno1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1,
				new FileContent(filename, "new2".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 2,
				new FileContent(filename, "new3".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		// do read here 
		ReplicaLoc[] replicas = masterServer.read(filename);

		if (replicas != null) {
			reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

			ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
					.lookup(FileSystem.REPLICAOBJECTNAME);

			FileContent data = replicaServer2.read(filename);

			System.out.println(new String(data.getData()));
		} else {
			System.out.println("File doesn't exist");
		}

		System.out.println(replicaServer.abort(msg.getTransactionId()));
		System.out.println(replicaServer.commit(msg.getTransactionId(), 3));

		// do read again 
		replicas = masterServer.read(filename);

		if (replicas != null) {
			reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

			ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
					.lookup(FileSystem.REPLICAOBJECTNAME);

			FileContent data = replicaServer2.read(filename);

			System.out.println(new String(data.getData()));
		} else {
			System.out.println("File doesn't exist");
		}
	}

	/**
	 * After transaction timeout no updates are done to the file
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 * @throws InterruptedException 
	 */
	public void test10() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException, InterruptedException {

		String filename = "filex.txt";
		Registry reg = LocateRegistry.getRegistry("whitechimera.no-ip.biz", 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		WriteMsg msg = masterServer.write(new FileContent(filename));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				filename, "msgno1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1,
				new FileContent(filename, "new2".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 2,
				new FileContent(filename, "new3".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		// do read here 
		ReplicaLoc[] replicas = masterServer.read(filename);

		reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

		ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		FileContent data = replicaServer2.read(filename);

		System.out.println(new String(data.getData()));

		// time out the transaction
		Thread.sleep(2000);
		System.out.println(replicaServer.commit(msg.getTransactionId(), 3));

		// do read again 
		replicas = masterServer.read(filename);

		reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

		replicaServer2 = (ReplicaServerClientInterface) reg.lookup(FileSystem.REPLICAOBJECTNAME);

		data = replicaServer2.read(filename);

		System.out.println(new String(data.getData()));
	}

	/**
	 * new file visible only when commit, not if transaction timed out 
	 * @throws NotBoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws MessageNotFoundException 
	 * @throws InterruptedException 
	 */
	public void test11() throws NotBoundException, FileNotFoundException, IOException,
			MessageNotFoundException, InterruptedException {

		String filename = "filexyz.txt";
		Registry reg = LocateRegistry.getRegistry("whitechimera.no-ip.biz", 9090);

		MasterServerClientInterface masterServer = (MasterServerClientInterface) reg
				.lookup(FileSystem.MASTEROBJECTNAME);

		WriteMsg msg = masterServer.write(new FileContent(filename));

		ReplicaLoc loc = msg.getLoc();

		reg = LocateRegistry.getRegistry(loc.getHost(), loc.getPort());

		ReplicaServerClientInterface replicaServer = (ReplicaServerClientInterface) reg
				.lookup(FileSystem.REPLICAOBJECTNAME);

		WriteMsg response = replicaServer.write(msg.getTransactionId(), 0, new FileContent(
				filename, "msgno1".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 1,
				new FileContent(filename, "new2".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		response = replicaServer.write(msg.getTransactionId(), 2,
				new FileContent(filename, "new3".getBytes()));

		System.out.println(response.getMsg() == WriteMsg.SUCCESS);

		// do read here 
		ReplicaLoc[] replicas = masterServer.read(filename);

		if (replicas != null) {
			reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

			ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
					.lookup(FileSystem.REPLICAOBJECTNAME);

			FileContent data = replicaServer2.read(filename);

			System.out.println(new String(data.getData()));
		} else {
			System.out.println("File doesn't exist");
		}

		// time out the transaction
		Thread.sleep(2000);
		System.out.println(replicaServer.commit(msg.getTransactionId(), 3));

		// do read again 
		replicas = masterServer.read(filename);

		if (replicas != null) {
			reg = LocateRegistry.getRegistry(replicas[0].getHost(), replicas[0].getPort());

			ReplicaServerClientInterface replicaServer2 = (ReplicaServerClientInterface) reg
					.lookup(FileSystem.REPLICAOBJECTNAME);

			FileContent data = replicaServer2.read(filename);

			System.out.println(new String(data.getData()));
		} else {
			System.out.println("File doesn't exist");
		}
	}
}
