import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class StartMasterServer {

	/**
	 * args[0] should be host name of master server
	 * args[1] should be port number of registry
	 * args[2] should be port number of master server object
	 * args[3] should be either to continue from crash or start over
	 * @param args
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static void main(String[] args) throws NumberFormatException, IOException {
		if (args.length < 4) {

			System.out
					.println("Usage: java -jar  replica.jar host registerportnumber objectportnumber false");
			return;
		}

		System.setProperty("java.rmi.server.hostname", args[0]);

		// create registry
		Registry reg = LocateRegistry.createRegistry(Integer.parseInt(args[1]));

		// create and bind object
		MasterServer master = new MasterServer(args[0], Integer.parseInt(args[1]),
				Integer.parseInt(args[2]), Boolean.parseBoolean(args[3]));
		reg.rebind(FileSystem.MASTEROBJECTNAME, master);

		// create and bind master manager object
		System.out.println("Master server started");
	}
}
