/**
 * This class carries a master lock record
 * @author aamer
 *
 */
public class MasterLogRecord {
	public static final int Request = 0x01;
	public static final int ACK = 0x02;

	private Transaction transaction;
	private int transactionType;

	public MasterLogRecord(Transaction t, int l) {
		transaction = t;
		transactionType = l;
	}

	public int getLockType() {
		return transactionType;
	}

	public void setTransactionType(int ttype) {
		transactionType = ttype;
	}

	public String toString() {
		return String.format("%20d %5d %20s %3d %3d", transaction.getTimestamp(),
				transaction.getId(), transaction.getFileName(), transaction.getType(), transactionType);
	}
}
