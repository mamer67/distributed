import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;

public class ReplicaServer extends UnicastRemoteObject implements ReplicaServerClientInterface,
		ReplicaMasterManagerInterface {

	private static final long serialVersionUID = 0x2L;

	public static final long MAXTIMEOUT = 1000000000;

	// used to store file and its corresponding path
	private ConcurrentHashMap<String, String> fileTable = new ConcurrentHashMap<String, String>(100);

	// used to store file and its corresponding path
	private ConcurrentHashMap<String, FileLock> filelocks = new ConcurrentHashMap<String, FileLock>(
			100);

	// used to store current ongoing transaction on replica
	private ConcurrentHashMap<Long, Tuple> transactions = new ConcurrentHashMap<Long, Tuple>(100);

	// history of transaction
	private PriorityBlockingQueue<Transaction> queue = new PriorityBlockingQueue<Transaction>();

	// path of current working directory
	private String directoryPath;

	// open the log
	private ReplicaLog log;

	public ReplicaServer(int port, String path, boolean crash) throws RemoteException {
		super(port);

		directoryPath = path;

		listAllFilesInDirectory(path);

		// begin crash handling here
		if (crash) {
			// TODO crash handling
		} else {
			log = new ReplicaLog(false, port);
		}

		// time out for transactions
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					timeOutTransaction();
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						System.out.println("Error in timeout thread");
					}
				}
			}
		}).start();
	}

	private void timeOutTransaction() {
		Enumeration<Tuple> tples = transactions.elements();

		while (tples.hasMoreElements()) {
			Tuple tpl = tples.nextElement();

			Long curr = System.nanoTime();

			if (!tpl.isRunning() && (curr - tpl.getTransaction().getTimestamp()) > MAXTIMEOUT) {
				doAbort(tpl.getTransaction().getId());
			}
		}
	}

	@Override
	public boolean bingReplica() throws RemoteException {
		return true;
	}

	@Override
	public String[] getFileNames() throws RemoteException {
		String[] fileNames = new String[fileTable.size()];

		Enumeration<String> files = fileTable.keys();
		int i = 0;

		while (files.hasMoreElements()) {
			fileNames[i++] = files.nextElement();
		}

		return fileNames;
	}

	// list all files inside a directory
	private void listAllFilesInDirectory(String directoryName) {
		File directory = new File(directoryName);

		// get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				fileTable.put(file.getName(), file.getAbsolutePath());
				filelocks.put(file.getName(), new FileLock(file.getName()));
			} else if (file.isDirectory()) {
				listAllFilesInDirectory(file.getAbsolutePath());
			}
		}
	}

	@Override
	public void setReplicasToPrimary(Transaction trx, ArrayList<ReplicaLoc> rpls, boolean newFile,
			String mh, int p) throws RemoteException {
		// add the tuple to concurrent transactions
		if (!transactions.containsKey(trx.getId())) {
			Tuple t = new Tuple(trx, rpls, newFile, true, mh, p);
			transactions.put(trx.getId(), t);

			// add to queue
			queue.add(trx);

			if (newFile) {
				// add the file path
				fileTable.put(trx.getFileName(), directoryPath + "/" + trx.getFileName());
				// create new Lock file for it
				filelocks.put(trx.getFileName(), new FileLock(trx.getFileName()));
			}
		} else {
			System.out.println("Warning multiple transactions found with same id");
		}
	}

	@Override
	public WriteMsg write(long txnID, long msgSeqNum, FileContent data) throws RemoteException,
			IOException {

		// create write transaction
		WriteTransaction wt = new WriteTransaction(msgSeqNum, data);

		// get current trx tuple
		Tuple tuple = transactions.get(txnID);

		if (tuple == null) {
			Transaction trx = new Transaction(txnID, Transaction.WRITE, data.getFileName());
			trx.setTimestamp(data.getTimestamp());

			// replica is just writing, from primary replica as client
			tuple = new Tuple(trx, null, false, false);

			// add the tuple
			transactions.put(txnID, tuple);

			// add to queue
			queue.add(trx);

			// acquire lock when its not primary
			FileLock fl = filelocks.get(tuple.getFileName());

			if (fl == null) {
				// add the file path
				fileTable.put(tuple.getFileName(), directoryPath + "/" + tuple.getFileName());
				// new file is being added so add new lock to it
				fl = new FileLock(tuple.getFileName());
				filelocks.put(tuple.getFileName(), fl);
			}

			while (queue.peek().getId() != txnID && queue.peek() != null)
				;
			fl.AcquireWriteLock();
		}

		WriteMsg response = new WriteMsg(txnID, System.nanoTime());

		// try adding message
		if (tuple.addWriteTransaction(wt)) {
			// correct seqno proceed
			response.setMsg(WriteMsg.SUCCESS);

			// create log
			ReplicaLogRecord logr = new ReplicaLogRecord(new Transaction(txnID, Transaction.WRITE,
					data.getFileName()), ReplicaLogRecord.Request, txnID, data.getData());

			// write log record
			log.writeRecord(logr);

			// do write operion

			// write log again
			logr.setTransactionType(ReplicaLogRecord.ACK);

			// write log record
			log.writeRecord(logr);

		} else {
			response.setMsg(WriteMsg.WRONGSEQNO);
		}

		return response;
	}

	@Override
	public FileContent read(String fileName) throws FileNotFoundException, IOException,
			RemoteException {
		// transfer entire file

		// just clear current transaction in history
		ReplicaLogRecord logr = new ReplicaLogRecord(
				new Transaction(-1, Transaction.READ, fileName), ReplicaLogRecord.Request);

		FileContent fc = doRead(fileName);

		// change logr
		logr.setTransactionType(ReplicaLogRecord.ACK);
		log.writeRecord(logr);

		return fc;
	}

	// do read
	private FileContent doRead(String fileName) {
		// take lock
		FileLock fl = filelocks.get(fileName);

		fl.AcquireReadLock();

		byte[] data = null;

		try {
			File f = new File(directoryPath + "/" + fileName);
			// open stream
			FileInputStream file = new FileInputStream(f);

			data = new byte[(int) f.length()];

			file.read(data);

			file.close();
		} catch (Exception e) {
			System.out.println("Error writing to file");
		}

		// create new file content
		FileContent fc = new FileContent(fileName, data);

		// release lock
		fl.ReleaseReadLock();

		return fc;
	}

	@Override
	public boolean commit(long txnID, long numOfMsgs) throws MessageNotFoundException,
			RemoteException {
		// get current trx tuple
		Tuple tuple = transactions.get(txnID);

		if (tuple == null) {
			// no transaction with that id
			return false;
		} else {
			// just clear current transaction in history
			ReplicaLogRecord logr = new ReplicaLogRecord(new Transaction(txnID, Transaction.COMMIT,
					tuple.getFileName()), ReplicaLogRecord.Request);

			boolean status = doCommit(tuple, numOfMsgs);

			// change logr
			logr.setTransactionType(ReplicaLogRecord.ACK);
			log.writeRecord(logr);

			return status;
		}
	}

	// do commit
	private Integer replicasUpdated = 0;

	private boolean doCommit(Tuple t, long numOfMsgs) {
		// set current transaction as running
		t.setRunning(true);

		while (queue.peek().getId() != t.getTransaction().getId() && queue.peek() != null)
			;

		replicasUpdated = 0;
		ArrayList<ReplicaLoc> rpls = t.getReplicas();

		// get the messages
		final int numberOfMessages = t.getMsgNo();
		final ArrayList<WriteTransaction> messages = t.getMessages();
		final long txid = t.getTransaction().getId();

		if (numberOfMessages != numOfMsgs) {
			return false;
		} else {

			if (t.isPrimary()) {
				// take locks on all other replicas
				for (int i = 0; i < rpls.size(); i++) {
					final ReplicaLoc repl = rpls.get(i);

					new Thread(new Runnable() {

						@Override
						public void run() {
							// start communicating with replicas
							Registry reg = null;

							try {
								reg = LocateRegistry.getRegistry(repl.getHost(), repl.getPort());
							} catch (RemoteException e) {
								//
								System.out.println("Error communicating with primary server");
							}

							ReplicaServerClientInterface replicaServer = null;
							try {
								replicaServer = (ReplicaServerClientInterface) reg
										.lookup(FileSystem.REPLICAOBJECTNAME);

								// flush data on replicas
								for (int i = 0; i < messages.size(); i++) {
									WriteTransaction wtc = messages.get(i);

									try {
										replicaServer.write(txid, wtc.getSeqno(), wtc.getFc());
									} catch (IOException e) {
										System.out.println("Error writing to replica");
									}
								}

								synchronized (replicasUpdated) {
									replicasUpdated++;
								}
							} catch (RemoteException | NotBoundException e) {
								//
								System.out.println("Error communicating with replica server");
							}
						}
					}).start();
				}
			}

			// take lock on my self, if I am primary, don't take lock if I am replica
			FileLock fl = filelocks.get(t.getFileName());

			// request write lock, if primary
			if (t.isPrimary()) {
				// check if replicas has lock , and we should wait for replicas to has lock as well
				while (true) {
					synchronized (replicasUpdated) {
						if (replicasUpdated == rpls.size()) {
							break;
						}
					}
				}

				// if replicas are successful in acquiring lock, then primary can acquire lock
				fl.AcquireWriteLock();
			}

			// flush data here, for my self
			for (int i = 0; i < messages.size(); i++) {
				WriteTransaction wtc = messages.get(i);
				byte[] data = wtc.getFc().getData();

				try {
					FileOutputStream fout = new FileOutputStream(directoryPath + "/"
							+ t.getFileName(), true);
					fout.write(data);
					fout.close();
				} catch (Exception e) {
					System.out.println("Error writing to a file");
				}
			}

			if (t.isPrimary() && t.isNewFile()) {
				// if data is new add this to master
				// start communicating with replicas
				Registry reg = null;

				try {
					reg = LocateRegistry.getRegistry(t.getHost(), t.getPort());
				} catch (RemoteException e) {
					//
					System.out.println("Error communicating with Master server");
				}

				MasterReplicaMangerInterface masterServer = null;
				try {
					masterServer = (MasterReplicaMangerInterface) reg
							.lookup(FileSystem.MASTEROBJECTNAME);

					masterServer.addFileToMetaData(t.getFileName(), rpls);
				} catch (RemoteException | NotBoundException e) {
					//
					System.out.println("Error communicating with replica server");
				}
			}

			// commit changes and release locks on other replicas
			if (t.isPrimary()) {
				replicasUpdated = 0;

				// take locks on all other replicas
				for (int i = 0; i < rpls.size(); i++) {
					final ReplicaLoc repl = rpls.get(i);

					new Thread(new Runnable() {

						@Override
						public void run() {
							// start communicating with replicas
							Registry reg = null;

							try {
								reg = LocateRegistry.getRegistry(repl.getHost(), repl.getPort());
							} catch (RemoteException e) {
								//
								System.out.println("Error communicating with primary server");
							}

							ReplicaServerClientInterface replicaServer = null;
							try {
								replicaServer = (ReplicaServerClientInterface) reg
										.lookup(FileSystem.REPLICAOBJECTNAME);

								// commit on other replica
								try {
									replicaServer.commit(txid, numberOfMessages);
								} catch (MessageNotFoundException e) {
									System.out.println("Message doesn't have same count");
								}

								synchronized (replicasUpdated) {
									replicasUpdated++;
								}
							} catch (RemoteException | NotBoundException e) {
								//
								System.out.println("Error communicating with replica server");
							}
						}
					}).start();
				}
			}

			// wait for other replicas, if primary
			if (t.isPrimary()) {
				// at this point primary should wait for replicas to commit 
				while (true) {
					synchronized (replicasUpdated) {
						if (replicasUpdated == rpls.size()) {
							break;
						}
					}
				}
			}

			// TODO remember this somehow
			// remove trx from tuple
			transactions.remove(txid);

			// release lock on current replica
			fl.ReleaseWriteLock();

			// remove tx from queue
			queue.poll();

			return true;
		}
	}

	@Override
	public boolean abort(long txnID) throws RemoteException {

		// just clear current transaction in history
		ReplicaLogRecord logr = new ReplicaLogRecord(new Transaction(txnID, Transaction.ABORT, ""),
				ReplicaLogRecord.Request);

		// write log
		log.writeRecord(logr);

		// do the abort operation
		boolean status = doAbort(txnID);

		// change logr
		logr.setTransactionType(ReplicaLogRecord.ACK);
		log.writeRecord(logr);

		return status;
	}

	private boolean doAbort(long txnId) {
		if (transactions.containsKey(txnId)) {
			Tuple tuple = transactions.remove(txnId);

			if (tuple.isNewFile()) {
				// remove respecting lock
				filelocks.remove(tuple.getFileName());
			}
			return true;
		} else {
			return false;
		}
	}
}
