import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class MasterLog {

	private PrintWriter masterLog;
	private PrintWriter masterCrashLog;

	private String masterLogName = "masterlog";
	private String masterCrashLogName = "mastercrashlog";

	public MasterLog(boolean resume) {
		try {
			masterLog = new PrintWriter(new FileWriter(masterLogName, true), true);

			if (!resume)
				masterCrashLog = new PrintWriter(new FileWriter(masterCrashLogName), true);

		} catch (Exception e) {
			System.out.println("Error opening file in ReplicaLog");
		}
	}

	public String getCrashLog() {
		return masterCrashLogName;
	}

	public void initializeCrashLog() {
		try {
			masterCrashLog = new PrintWriter(new FileWriter(masterCrashLogName), true);
		} catch (Exception e) {
			System.out.println("Error opening file in ReplicaLog");
		}
	}

	public void writeRecord(MasterLogRecord r) {
		masterLog.println(r.toString());
		masterCrashLog.println(r.toString());
	}

	public void close() {
		masterLog.close();
		masterCrashLog.close();

		// delete crash log
		File f = new File(masterCrashLogName);
		f.delete();
	}
}
