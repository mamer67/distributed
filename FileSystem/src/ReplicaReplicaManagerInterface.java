import java.rmi.Remote;

/**
 * This interface helps replica to communicate with each other
 * @author aamer
 *
 */
public interface ReplicaReplicaManagerInterface extends Remote {

}
